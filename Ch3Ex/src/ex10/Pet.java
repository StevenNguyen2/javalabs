package ex10;

public class Pet {
	//instance fields
	private String name;
	private String type;
	private int age;
	
	//mutators
	public void setName(String name)
	{
		this.name = name;
	}
	
	public void setType(String type)
	{
		this.type = type;
	}
	
	public void setAge(int age)
	{
		this.age = age;
	}
	
	//accessors
	public String getName()
	{
		return name;
	}
	
	public String getType()
	{
		return type;
	}
	
	public int getAge()
	{
		return age;
	}
}
