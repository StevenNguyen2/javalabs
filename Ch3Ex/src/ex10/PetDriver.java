package ex10;

import java.util.Scanner;

public class PetDriver {

	public static void main(String[] args) {
		//variables
		String name;
		String type;
		int age;
		
		//instantiate Pet object
		Pet p1 = new Pet();
		
		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		
		//prompt user
		System.out.println("What is your pet's name?");
		name = keyboard.nextLine();
		
		System.out.println("What is your pet's type?");
		type = keyboard.nextLine();
	
		System.out.println("What is your pet's age?");
		age = keyboard.nextInt();
		
		//set attributes
		p1.setName(name);
		p1.setType(type);
		p1.setAge(age);
		
		//output
		System.out.println("Pet's name: " + p1.getName());
		System.out.println("Pet's type: " + p1.getType());
		System.out.println("Pet's age: " + p1.getAge());
	}

}
