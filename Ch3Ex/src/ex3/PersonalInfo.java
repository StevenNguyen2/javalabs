package ex3;

public class PersonalInfo {
	//instance fields
	private String name;
	private String address;
	private int age;
	private int phoneNumber;
	
	//mutator
	public void setName(String name) 
	{
		this.name = name;
	}
	
	public void setAddress(String address)
	{
		this.address = address;
	}
	
	public void setAge(int age)
	{
		this.age = age;
	}
	
	public void setNumber(int phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}
	
	//accessor
	public String getName()
	{
		return name;
	}
	
	public String getAddress()
	{
		return address;
	}
	
	public int getAge()
	{
		return age;
	}
	
	public int getNumber()
	{
		return phoneNumber;
	}
}
