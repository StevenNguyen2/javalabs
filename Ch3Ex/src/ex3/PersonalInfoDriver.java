package ex3;

public class PersonalInfoDriver {

	public static void main(String[] args) {
		//instantiate PersonalInfo object
		PersonalInfo p1 = new PersonalInfo();
		PersonalInfo p2 = new PersonalInfo();
		PersonalInfo p3 = new PersonalInfo();
		
		//call the mutator
		p1.setName("Steven");
		p1.setAddress("8824 Green Crest Lane");
		p1.setAge(21);
		p1.setNumber(7753195);
		
		p2.setName("Mom");
		p2.setAddress("8824 Green Crest Lane");
		p2.setAge(53);
		p2.setNumber(1234567);
		
		p3.setName("Dad");
		p3.setAddress("8824 Green Crest Lane");
		p3.setAge(51);
		p3.setNumber(9876541);
		
		//call the accessor
		System.out.println(p1.getName());
		System.out.println(p1.getAddress());
		System.out.println(p1.getAge());
		System.out.println(p1.getNumber());
		
		System.out.println("");
		
		System.out.println(p2.getName());
		System.out.println(p2.getAddress());
		System.out.println(p2.getAge());
		System.out.println(p2.getNumber());
		
		System.out.println("");
		
		System.out.println(p3.getName());
		System.out.println(p3.getAddress());
		System.out.println(p3.getAge());
		System.out.println(p3.getNumber());
	}

}
