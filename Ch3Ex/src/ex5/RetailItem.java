package ex5;

public class RetailItem {
	//instance fields
	private String description;
	private int unitsOnHand;
	private double price;
	
	//mutator
	public void setDescription(String description)
	{
		this.description = description;
	}
	
	public void setUnits(int unitsOnHand)
	{
		this.unitsOnHand = unitsOnHand;
	}
	
	public void setPrice(double price)
	{
		this.price = price;
	}
	
	//accessor
	public String getDescription()
	{
		return description;
	}
	
	public int getUnits()
	{
		return unitsOnHand;
	}
	
	public double getPrice()
	{
		return price;
	}
}
