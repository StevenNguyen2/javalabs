package ex5;

public class RetailItemDriver {

	public static void main(String[] args) {
		//instantiate an object
		RetailItem item1 = new RetailItem();
		RetailItem item2 = new RetailItem();
		RetailItem item3 = new RetailItem();
		
		//set attributes
		item1.setDescription("Jacket");
		item1.setUnits(12);
		item1.setPrice(59.95);
		
		item2.setDescription("Designer Jeans");
		item2.setUnits(40);
		item2.setPrice(34.95);
		
		item3.setDescription("Shirt");
		item3.setUnits(20);
		item3.setPrice(24.95);
		
		//call getters
		System.out.println(item1.getDescription());
		System.out.println(item1.getUnits());
		System.out.println(item1.getPrice());
		
		System.out.println("");
		
		System.out.println(item2.getDescription());
		System.out.println(item2.getUnits());
		System.out.println(item2.getPrice());
		
		System.out.println("");
		
		System.out.println(item3.getDescription());
		System.out.println(item3.getUnits());
		System.out.println(item3.getPrice());
	}

}
