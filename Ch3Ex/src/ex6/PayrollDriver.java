package ex6;
import java.util.Scanner;

public class PayrollDriver {

	public static void main(String[] args) {
		//variables
		int hoursWorked;
		double hourlyRate;
		
		//instantiate an object
		Payroll employee1 = new Payroll();
		
		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user
		System.out.println("Enter total hours worked");
		hoursWorked = keyboard.nextInt();
	
		System.out.println("Enter hourly rate");
		hourlyRate = keyboard.nextDouble();
		
		//set attribute of object employee1 to keyboard input
		employee1.setHoursWorked(hoursWorked);
		employee1.setRate(hourlyRate);
		
		//display employee info
		System.out.printf("Total: $%,.2f\n", employee1.calcGrossPay());
	}

}
