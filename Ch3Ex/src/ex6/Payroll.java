package ex6;

public class Payroll {
	//instance fields
	private String employeeName;
	private int idNumber;
	private double hourRate;
	private int hoursWorked;
	
	//mutator
	public void setRate(double hourRate)
	{
		this.hourRate = hourRate;
	}
	
	public void setHoursWorked(int hoursWorked)
	{
		this.hoursWorked = hoursWorked;
	}
	
	//default constructor
	public Payroll()
	{
		
	}
	
	//constructor with 2 parameters
	public Payroll(String employeeName, int idNumber)
	{
		this.employeeName = employeeName;
		this.idNumber = idNumber;
	}
	
	//accessors
	public String getName()
	{
		return employeeName;
	}
	
	public int getId()
	{
		return idNumber;
	}
	
	public double getHourRate()
	{
		return hourRate;
	}
	
	public int getHoursWorked()
	{
		return hoursWorked;
	}
	
	//gross pay method
	public double calcGrossPay()
	{
		double grossPay = hoursWorked * hourRate;
		return grossPay;
	}
}
