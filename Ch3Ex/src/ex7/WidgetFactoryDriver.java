package ex7;

public class WidgetFactoryDriver {

	public static void main(String[] args) {
		//instantiate an object
		WidgetFactory w1 = new WidgetFactory(500);
		
		//output
		System.out.println("500 widgets take " + w1.calcDays() + " days to make");
	}

}
