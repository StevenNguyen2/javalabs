package ex7;

public class WidgetFactory {
	//instance fields
	private double daysToProduce;
	private double widgetsRequired;
	
	//default constructor
	public WidgetFactory()
	{
		
	}
	
	//constructor/method to calculate days it'll take
	public WidgetFactory(int widgetsRequired)
	{
		this.widgetsRequired = widgetsRequired;
	}
	
	//method to calculate how many days it'll take
	public double calcDays()
	{
		daysToProduce = widgetsRequired / 160.0;
		return daysToProduce;
	}
	
}
