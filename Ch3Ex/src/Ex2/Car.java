package Ex2;

public class Car {
	//instance fields
	private int yearModel;
	private String make;
	private int speed;
	
	//default constructor
	public Car()
	{
		
	}
	
	//constructor w/ 2 parameters
	public Car(int yearModel, String make)
	{
		this.yearModel = yearModel;
		this.make = make;
		speed = 0;
	}
	
	//accessor
	public int getModel()
	{
		return yearModel;
	}
	
	public String getMake()
	{
		return make;
	}
	
	public int getSpeed()
	{
		return speed;
	}
	
	//accelerate method
	public void accelerateSpeed()
	{
		speed += 5;
	}
	
	//brake method
	public void brakeMethod()
	{
		speed -= 5;
	}
}
