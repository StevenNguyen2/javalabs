package ex4;

public class Temperature {
	//instance fields
	private double ftemp;
	
	//default constructor
	public Temperature()
	{
		
	}
	
	//constructor w/ 1 parameter
	public Temperature(double ftemp)
	{
		this.ftemp = ftemp;
	}
	
	//method that accepts a fahrenheit
	public void setFahrenheit(double fahrenheit)
	{
		ftemp = fahrenheit;
	}
	
	//method that returns the value of the ftemp field
	public double getFahrenheit()
	{
		return ftemp;
	}
	
	//method that returns the value of ftemp field converted to celsius
	public double getCelsius()
	{
		double celsius = (5.0/9.0) * (ftemp - 32);
		return celsius;
	}
	
	public double getKelvin()
	{
		double kelvin = ((5.0/9.0) * (ftemp - 32)) + 273;
		return kelvin;
	}
}
