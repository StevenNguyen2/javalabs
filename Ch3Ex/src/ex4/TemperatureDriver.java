package ex4;
import java.util.Scanner;

public class TemperatureDriver {

	public static void main(String[] args) {
		//variables
		double temp;
		
		//instantiate a Temperature object
		Temperature t1 = new Temperature();
		
		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
				
		//prompt user
		System.out.println("Enter a temperature in fahrenheit");
		temp = keyboard.nextDouble();
		
		//set attribute (fahrenheit) of object t1 to keyboard input
 		t1.setFahrenheit(temp);
		
		//display temperature in celsius and kelvin
		System.out.println(t1.getFahrenheit());
		System.out.println(t1.getCelsius());
		System.out.println(t1.getKelvin());
		
		keyboard.close();
	}

}
