package ex8;

public class TestScores {
	//instance fields
	public double testScore1;
	public double testScore2;
	public double testScore3;
	
	//mutators
	public void setTestScore1(int testScore1)
	{
		this.testScore1 = testScore1;
	}
	
	public void setTestScore2(int testScore2)
	{
		this.testScore2 = testScore2;
	}
	
	public void setTestScore3(int testScore3)
	{
		this.testScore3 = testScore3;
	}
	
	//accessors
	public double getTestScore1()
	{
		return testScore1;
	}
	
	public double getTestScore2()
	{
		return testScore2;
	}
	
	public double getTestScore3()
	{
		return testScore3;
	}
}
