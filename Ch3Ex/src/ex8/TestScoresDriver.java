package ex8;
import java.util.Scanner;

public class TestScoresDriver {

	public static void main(String[] args) {
		//variables
		double testScore1;
		double testScore2;
		double testScore3;
		double testAverage;
		
		//instantiate TestScores object
		TestScores test1 = new TestScores();
		
		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user
		System.out.println("Enter the first test score");
		testScore1 = keyboard.nextDouble();
		
		System.out.println("Enter the second test score");
		testScore2 = keyboard.nextDouble();
		
		System.out.println("Enter the third test score");
		testScore3 = keyboard.nextDouble();
		
		//output
		testAverage = (testScore1 + testScore2 + testScore3) / 3;
		System.out.println("The average of the scores: " + testAverage);
	}

}
