package ex9;

public class Circle {
	//instance fields
	public double radius;
	public final double PI = 3.14159;

	//constructor
	public Circle(double radius)
	{
		this.radius = radius;
	}
	
	//mutator
	public void setRadius()
	{
		this.radius = radius;
	}
	
	//accessor
	public double getRadius()
	{
		return radius;
	}
	
	public double getArea()
	{
		double area = PI * radius * radius;
		return area;
	}
	
	public double getDiameter()
	{
		double diameter = radius * 2.0;
		return diameter;
	}
	
	public double getCircumference()
	{
		double circumference = 2.0 * PI * radius;
		return circumference;
	}
}
