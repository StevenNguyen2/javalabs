package ex9;
import java.util.Scanner;

public class CircleDriver {

	public static void main(String[] args) {
		//variables
		double radius;
		
		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user
		System.out.println("What is the circle's radius?");
		radius = keyboard.nextDouble();
		
		//instantiate Circle object
		Circle c1 = new Circle(radius);
		
		System.out.println("Circle's area: " + c1.getArea());
		System.out.println("Circle's diameter: " + c1.getDiameter());
		System.out.println("Circle's circumference: " + c1.getCircumference());
	}

}
