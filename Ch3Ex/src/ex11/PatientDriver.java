package ex11;

public class PatientDriver {

	public static void main(String[] args) {
		//create 3 instances of the Procedure class
		Patient patient1 = new Patient();
		Procedure p1 = new Procedure();
		Procedure p2 = new Procedure();
		Procedure p3 = new Procedure();
		
		//set attributes of patient1
		patient1.setFirstName("Steven");
		patient1.setMiddleName("None");
		patient1.setLastName("Nguyen");
		patient1.setAddress("8824 Green Crest Lane");
		patient1.setCity("St. Louis");
		patient1.setState("MO");
		patient1.setZipCode("63126");
		patient1.setPhoneNumber("314-775-3195");
		patient1.setEmergencyName("Mom");
		patient1.setEmergencyContact("314-729-1942");
		
		//output of patient1
		System.out.println("Patient:");
		System.out.println("First Name: " + patient1.getFirstName());
		System.out.println("Middle Name: " + patient1.getMiddleName());
		System.out.println("Last Name: " + patient1.getLastName());
		System.out.println("Address: " + patient1.getAddress());
		System.out.println("City: " + patient1.getCity());
		System.out.println("State: " + patient1.getState());
		System.out.println("Zip Code: " + patient1.getZipCode());
		System.out.println("Phone Number: " + patient1.getPhoneNumber());
		System.out.println("Emergency Name: " + patient1.getEmergencyName());
		System.out.println("Emergency Contact: " + patient1.getEmergencyContact());
		System.out.println("");
		
		//set attributes of p1/p2/p3
		p1.setName("Physical Exam");
		p1.setDate("1/22/2018");
		p1.setNameOfPractitioner("Dr. Irvine");
		p1.setCharges("250.00");
		
		p2.setName("X-Ray");
		p2.setDate("1/22/2018");
		p2.setNameOfPractitioner("Dr. Jamison");
		p2.setCharges("500.00");
		
		p3.setName("Blood test");
		p3.setDate("1/22/2018");
		p3.setNameOfPractitioner("Dr. Smith");
		p3.setCharges("200.00");
		
		//output of Procedure 1
		System.out.println("Procedure 1:");
		System.out.println("Procedure Name: " + p1.getName());
		System.out.println("Date: " + p1.getDate());
		System.out.println("Practitioner: " + p1.getNameOfPractitioner());
		System.out.println("Charge: $" + p1.getCharges());
		System.out.println("");
		
		//output of Procedure 2
		System.out.println("Procedure 2:");
		System.out.println("Procedure Name: " + p2.getName());
		System.out.println("Date: " + p2.getDate());
		System.out.println("Practitioner: " + p2.getNameOfPractitioner());
		System.out.println("Charge: $" + p2.getCharges());
		System.out.println("");
		
		//output of Procedure 3
		System.out.println("Procedure 3");
		System.out.println("Procedure Name: " + p3.getName());
		System.out.println("Date: " + p3.getDate());
		System.out.println("Practitioner: " + p3.getNameOfPractitioner());
		System.out.println("Charge: $" + p3.getCharges());
	}

}
