package ex11;

public class Procedure {
	//instance fields
	private String name;
	private String date;
	private String nameOfPractitioner;
	private String charges;
	
	//constructor
	public Procedure(String name, String date, String nameOfPractitioner, String charges)
	{
		this.name = name;
		this.date = date;
		this.nameOfPractitioner = nameOfPractitioner;
		this.charges = charges;
	}
	
	//default constructor
	public Procedure()
	{
		
	}
	
	//mutators
	public void setName(String name)
	{
		this.name = name;
	}
	
	public void setDate(String date)
	{
		this.date = date;
	}
	
	public void setNameOfPractitioner(String nameOfPractitioner)
	{
		this.nameOfPractitioner = nameOfPractitioner;
	}
	
	public void setCharges(String charges)
	{
		this.charges = charges;
	}
	
	//accessors
	public String getName()
	{
		return name;
	}
	
	public String getDate()
	{
		return date;
	}
	
	public String getNameOfPractitioner()
	{
		return nameOfPractitioner;
	}
	
	public String getCharges()
	{
		return charges;
	}
}
