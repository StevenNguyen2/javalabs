package Ex1;

public class EmployeeDriver {
	public static void main(String[] args)
	{
		//instantiate an employee object
		Employee susanMeyers = new Employee();
		Employee markJones = new Employee();
		Employee joyRogers = new Employee();
		
		//call the mutator
		susanMeyers.setName("Susan Meyers");
		susanMeyers.setId(47899);
		susanMeyers.setDepartment("Accounting");
		susanMeyers.setPosition("Vice President");
		
		markJones.setName("Mark Jones");
		markJones.setId(39119);
		markJones.setDepartment("IT");
		markJones.setPosition("Programmer");
		
		joyRogers.setName("Joy Rogers");
		joyRogers.setId(81774);
		joyRogers.setDepartment("Manufacturing");
		joyRogers.setPosition("Engineer");
		
		//call the accessor
		System.out.println(susanMeyers.getName());
		System.out.println(susanMeyers.getId());
		System.out.println(susanMeyers.getDepartment());
		System.out.println(susanMeyers.getPosition());
		System.out.println("");
		
		System.out.println(markJones.getName());
		System.out.println(markJones.getId());
		System.out.println(markJones.getDepartment());
		System.out.println(markJones.getPosition());
		System.out.println("");

		System.out.println(joyRogers.getName());
		System.out.println(joyRogers.getId());
		System.out.println(joyRogers.getDepartment());
		System.out.println(joyRogers.getPosition());
	}
	
}
