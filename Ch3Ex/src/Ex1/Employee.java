package Ex1;

public class Employee {
	//instance fields
	private String name;
	private int idNumber;
	private String department;		private String position;
		
	//mutator
	public void setName(String employeeName)
	{
		name = employeeName;
	}
		
	//accessor
	public String getName()
	{
		return name;	
	}
		
	//mutator
	public void setId(int id)
	{
		idNumber = id;
	}
		
	//accessor
	public int getId()
	{
		return idNumber;
	}
		
	//mutator
	public void setDepartment(String departmentName)
	{
		department = departmentName;
	}
		
	//accessor
	public String getDepartment()
	{
		return department;
	}
		
	//mutator
	public void setPosition(String positionName)
	{
		position = positionName;
	}
		
	//accessor
	public String getPosition()
	{
		return position;
	}
}
