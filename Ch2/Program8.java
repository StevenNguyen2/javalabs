import java.util.Scanner;

public class Program8
{
	public static void main(String[] args)
	{
        double input;
        double answer;
        final double COUNTY_TAX = 0.02;
        final double STATE_TAX = 0.055;
        
        //create a scanner object for keyboard input
        Scanner keyboard = new Scanner(System.in);
        
        //prompt user
        System.out.println("Enter amount of purchase");
        input = keyboard.nextInt();
        
        //math
        double calcState = input * STATE_TAX;
        double calcCounty = input * COUNTY_TAX;
        double totalTax = calcState + calcCounty;
        
        //output
        System.out.println("Amount of purchase: " + input);
        System.out.println("County Sales Tax: " + (calcCounty));
        System.out.println("State Sales Tax: " + (calcState));
        System.out.println("Total Sales Tax: " + (totalTax));
        System.out.println("Total Amount: " + (totalTax + input));
	}
}