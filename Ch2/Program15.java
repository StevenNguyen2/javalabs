import java.util.Scanner;

public class Program15
{
    public static void main(String[] args)
    {
        double input;
        double sugar;
        double butter;
        double flour;
        
        //create scanner object for keyboard input
        Scanner keyboard = new Scanner(System.in);
        
        //prompt user
        System.out.println("How many cookies would you like to make?");
        input = keyboard.nextDouble();
        
        //OUTPUTS
        double percentage = (input / 48);
        
        sugar = 1.5 * percentage;
        System.out.println("Amount of sugar needed: " + sugar + " cups");
        
        butter = 1 * percentage;
        System.out.println("Amount of butter needed: " + butter + " cups");
        
        flour = 2.75 * percentage;
        System.out.println("Amount of flour needed: " + flour + " cups");
    }
}