import java.util.Scanner;

public class Program12
{
    public static void main(String[] args)
    {
        String input;
        int stringSize;
        String lowerCase;
        String upperCase;
        char letter;
        
        //create a scanner object for keyboard input
        Scanner keyboard = new Scanner(System.in);
        
        //prompt user
        System.out.println("Enter the name of your favorite city");
        input = keyboard.nextLine();
        
        //outputs
        //num of characters in city name
        stringSize = input.length();
        System.out.println("# of characters: " + stringSize);
        
        //name of city in all lowercase letters
        lowerCase = input.toLowerCase();
        System.out.println("Lowercase: " + lowerCase);
        
        //name of city in all uppercase letters
        upperCase = input.toUpperCase();
        System.out.println("Uppercase: " + upperCase);
        
        //first charafter in the name of the city
        letter = input.charAt(0);
        System.out.println("First character: " + letter);
    }
}