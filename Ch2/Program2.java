public class Program2
{
	public static void main(String[] args)
	{
        String firstName = "Steven";
        String middleName = "None";
        String lastName = "Nguyen";
        
        char firstInitial = 'S';
        char middleInitial = 'N';
        char lastInitial = 'N';
        
		System.out.println("My first name is " + firstName + " and my middle name is "
                           + middleName + " and my last name is " + lastName);
        
        System.out.println("My initials are " + firstInitial + middleInitial + lastInitial);
	}
}