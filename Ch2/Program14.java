import java.util.Scanner;

public class Program14
{
    public static void main(String[] args)
    {
        double paidAmount;
        double commissionAmount;
        double totalAmount;
        
        //OUTPUTS
        //amount paid for the stock w/o commission
        paidAmount = 25.50 * 1000;
        System.out.println("Amount paid: " + paidAmount);
        
        //amount of commission
        commissionAmount = paidAmount * .02;
        System.out.println("Commission amount: " + commissionAmount);
        
        //total amount paid
        totalAmount = paidAmount + commissionAmount;
        System.out.println("Total amount: " + totalAmount);
    }
}