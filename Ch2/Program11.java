import java.util.Scanner;

public class Program11
{
    public static void main(String[] args)
    {
        double maleInput;
        double femaleInput;
        
        //create a scanner object for keyboard input
        Scanner keyboard = new Scanner(System.in);
        
        //prompt user
        System.out.println("How many males are registered?");
        maleInput = keyboard.nextInt();
        
        System.out.println("How many females are registered?");
        femaleInput = keyboard.nextInt();
        
        //output
        double total = maleInput + femaleInput;
        
        System.out.println("Males: " + (maleInput/total) * 100) + "%";
        System.out.println("Females: " + (femaleInput/total) * 100) + "%";
    }
}