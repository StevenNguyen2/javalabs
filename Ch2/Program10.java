import java.util.Scanner;

public class Program10
{
	public static void main(String[] args)
	{
        int input;
        int secondInput;
        int thirdInput;
        double answer;

        //create a scanner object for keyboard input
        Scanner keyboard = new Scanner(System.in);
        
        //prompt user
        System.out.println("Enter a test score");
        input = keyboard.nextInt();
        
        System.out.println("Enter a second test score");
        secondInput = keyboard.nextInt();
        
        System.out.println("Enter a third test score");
        thirdInput = keyboard.nextInt();
        
        //output
        System.out.println("Test Score 1: " + input);
        System.out.println("Test Score 2: " + secondInput);
        System.out.println("Test Score 3: " + thirdInput);
        
        answer = ((input + secondInput + thirdInput) / 3);
        
        System.out.println("Average: " + answer);
	}
}