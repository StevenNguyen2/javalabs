import java.util.Scanner;

public class Program13
{
    public static void main(String[] args)
    {
        final double TAX_CHARGE = 0.075;
        final double MEAL_TIP = 0.18;
        double mealTotal;
        double input;
        double taxAmount;
        double tipAmount;
        double totalBill;

        
        //create scanner object for keyboard input
        Scanner keyboard = new Scanner(System.in);
        
        //prompt user
        System.out.println("Enter total of the meal");
        input = keyboard.nextDouble();
        
        //OUTPUTS
        //display meal charge
        System.out.println("Meal charge: " + input);
        
        //display tax amount
        taxAmount = input * TAX_CHARGE;
        System.out.println("Tax amount: " + taxAmount);
        
        //display tip amount
        tipAmount = input * MEAL_TIP;
        System.out.println("Tip amount: " + tipAmount);
        
        //display total bill
        totalBill = input + tipAmount + taxAmount;
        System.out.println("Total bill: " + totalBill);
    }
}