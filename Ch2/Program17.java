import java.util.Scanner;

public class Program17
{
    public static void main(String[] args)
    {
        String name;
        String age;
        String city;
        String college;
        String profession;
        String animal;
        String petName;
        
        //create scanner object for keyboard input
        Scanner keyboard = new Scanner(System.in);
        
        //prompt user
        System.out.println("Enter name");
        name = keyboard.nextLine();
        
        System.out.println("Enter age");
        age = keyboard.nextLine();
        
        System.out.println("Enter city");
        city = keyboard.nextLine();
        
        System.out.println("Enter college");
        college = keyboard.nextLine();
        
        System.out.println("Enter profession");
        profession = keyboard.nextLine();
        
        System.out.println("Enter type of animal");
        animal = keyboard.nextLine();
        
        System.out.println("Enter pet's name");
        petName = keyboard.nextLine();
        
        //outputs
        System.out.println("There was once a person named " + name +  " who lived in " + city + ". At the age of " + age + ", " + name + " went to college at " + college + ". " + name + " graduated and went to work as a " + profession + ". " + "Then, " + name + " adopted a(n) " + animal + " named " + petName + "." + " They both lived happily ever after!");
    }
}