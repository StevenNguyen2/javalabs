public class Program3
{
	public static void main(String[] args)
	{
        String name = "Steven";
        String address = "8824 Green Crest Lane, MO, 63126";
        long telephoneNumber = 3147753195l;
        String major = "Computer Science";
        
        System.out.println("My name is " + name 
                           + "\nMy address is " + address
                           + "\nMy number is " + telephoneNumber
                           + "\nMy college major is " + major);
	}
}