public class Program16
{
    public static void main(String[] args)
    {
        double numOfCustomers;
        double numOfCitrus;
        
        //output
        numOfCustomers = 15000 * 0.18;
        System.out.println("Number of customers who purchase one or more energy drinks per week: " + numOfCustomers);
        
        numOfCitrus = numOfCustomers * 0.58;
        System.out.println("Number of customers who prefer citrus-flavored energy drinks: " + numOfCitrus);
    }
}