import java.util.Scanner;

public class Program9
{
	public static void main(String[] args)
	{
        double milesInput;
        double gallonsInput;
        double answer;
        
        //create a scanner object for keyboard input
        Scanner keyboard = new Scanner(System.in);
        
        //ask user a question
        System.out.println("How many miles have you driven?");
        milesInput = keyboard.nextDouble();
        
        System.out.println("How many gallons have you used?");
        gallonsInput = keyboard.nextDouble();
        
        //output
        answer = (milesInput / gallonsInput);
        System.out.println(answer + " MPG");
	}
}