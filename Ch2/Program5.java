import java.util.Scanner;

public class Program5
{
	public static void main(String[] args)
	{
        double input;
        double answer;
        //4 cookies = 1 serving = 300 calories
        //1 cookie = 75 calories
        
        //create a scanner object for keyboard input
        Scanner keyboard = new Scanner(System.in);
        
        //ask user a question
        System.out.println("How many cookies did you eat?");
        input = keyboard.nextInt();
        
        //output
        answer = (input * 75);
        System.out.println(answer + " Calories");
	}
}