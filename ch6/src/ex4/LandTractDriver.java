package ex4;
import java.util.Scanner;

public class LandTractDriver {

	public static void main(String[] args) {
		//variables
		double length1;
		double length2;
		double width1;
		double width2;
		
		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user
		System.out.println("Enter length of first tract");
		length1 = keyboard.nextDouble();
		System.out.println("Enter width of first tract");
		width1 = keyboard.nextDouble();
		
		System.out.println("Enter length of second tract");
		length2 = keyboard.nextDouble();
		System.out.println("Enter width of second tract");
		width2 = keyboard.nextDouble();
		
		//instantiate an object & set attributes
		LandTract object1 = new LandTract(length1, width1);
		LandTract object2= new LandTract(length2, width2);
		
		//output
		if (object1.equals(object2)) 
		{
			System.out.println("The tracts are of equal size");
			System.out.println("Area of tract 1: " + object1.getTractArea());
			System.out.println("Area of tract 2: " + object2.getTractArea());
		}
		else
		{
			System.out.println("The tracts are not of equal size");
			System.out.println("Area of tract 1: " + object1.getTractArea());
			System.out.println("Area of tract 2: " + object2.getTractArea());
		}
		
	}

}
