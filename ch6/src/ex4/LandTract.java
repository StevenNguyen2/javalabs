package ex4;

public class LandTract {
	//instance fields
	private double length;
	private double width;
	
	//constructor
	public LandTract(double length, double width) 
	{
		this.length = length;
		this.width = width;
	}
	
	//accessor
	public double getLength()
	{
		return length;
	}
	
	public double getWidth()
	{
		return width;
	}
	
	//method that returns the tract's area
	public double getTractArea()
	{
		return length * width;
	}
	
	//equals method
	public boolean equals(LandTract object2)
	{
		boolean status;
		
		if (getTractArea() == object2.getTractArea())
		{
			status = true;
		}
		else
		{
			status = false;
		}
		return status;
	}
	
	//toString method
	public String toString()
	{
		return "\nThe width is " + width + "\nThe length is " + length;
	}
}
