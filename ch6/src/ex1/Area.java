package ex1;

public class Area {
	//circle
	public static double calcArea(double radius)
	{
		double area = Math.PI * (radius * radius);
		return area;
	}
	
	//rectangle
	public static double calcArea(double width, double length)
	{
		double area = width * length;
		return area;
	}
	
	//cylinder
	public static double calcArea(double area, double radius, double height)
	{
		area = Math.PI * (radius * radius) * height;
		return area;
	}
}
