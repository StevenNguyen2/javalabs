package ex1;

public class AreaDriver {

	public static void main(String[] args) {
		//output
		System.out.println(Area.calcArea(5));
		System.out.println(Area.calcArea(2, 4));
		System.out.println(Area.calcArea(6, 4, 5));
	}

}
