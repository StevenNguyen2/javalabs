package ex6;

public class EmployeeDriver {
	public static void main(String[] args)
	{
		//instantiate an employee object
		Employee steven = new Employee("Steven", 1234, "Swag", "Millionaire");
		Employee brokeboy = new Employee("Dude", 555);
		Employee nothing = new Employee();
		
		//output
		System.out.println(steven.toString());
		System.out.println(brokeboy.toString());
		System.out.println(nothing.toString());	
	}
	
}
