package ex6;

public class Employee {
	//instance fields
	private String name;
	private int idNumber;
	private String department;		private String position;
		
	//mutator
	public void setName(String employeeName)
	{
		name = employeeName;
	}
		
	//accessor
	public String getName()
	{
		return name;	
	}
		
	//mutator
	public void setId(int id)
	{
		idNumber = id;
	}
		
	//accessor
	public int getId()
	{
		return idNumber;
	}
		
	//mutator
	public void setDepartment(String departmentName)
	{
		department = departmentName;
	}
		
	//accessor
	public String getDepartment()
	{
		return department;
	}
		
	//mutator
	public void setPosition(String positionName)
	{
		position = positionName;
	}
		
	//accessor
	public String getPosition()
	{
		return position;
	}
	
	//A constructor that accepts the following values as arguments and assigns them to the appropriate fields:
	//employee’s name, employee’s ID number, department, and position.
	public Employee(String employeeName, int id, String department, String positionName)
	{
		name = employeeName;
		idNumber = id;
		this.department = department;
		position = positionName;
	}
	
	//A constructor that accepts the following values as arguments and assigns them to the appropriate fields: employee’s name and ID number. 
	//The department and position fields should be assigned an empty string ("").
	public Employee(String employeeName, int id)
	{
		name = employeeName;
		idNumber = id;
		department = "";
		position = "";
	}
	
	//A no-arg constructor that assigns empty strings ("") to the name, department, and
	//position fields, and 0 to the idNumber field.
	public Employee()
	{
		name = "";
		department = "";
		position = "";
		idNumber = 0;
	}
	
	public String toString()
	{
		return "The employees name is " + name + " and their id number is " + idNumber + " and department is " + department + " and the position is " + position;
	}
}
