package ex10;

public class ParkedCar {
	//instance fields
	private String carMake;
	private String model;
	private String color;
	private int licenseNum;
	private int carParked;
	
	//constructor
	public ParkedCar(String carMake, String model, String color, int licenseNum, int carParked)
	{
		this.carMake = carMake;
		this.model = model;
		this.color = color;
		this.licenseNum = licenseNum;
		this.carParked = carParked;
	}
	
	//accessor
	public String getCar()
	{
		return carMake;
	}
	
	public String getModel()
	{
		return model;
	}
	
	public String getColor()
	{
		return color;
	}
	
	public int getLicenseNum()
	{
		return licenseNum;
	}
	
	public int getCarParked()
	{
		return carParked;
	}
}
