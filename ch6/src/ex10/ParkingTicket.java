package ex10;

public class ParkingTicket {
	String carMake;
	String model;
	String color;
	int licenseNum;
	int parkingTime;
	
	//– To report the make, model, color, and license number of the illegally parked car
	public ParkingTicket(ParkedCar object2)
	{
		carMake = object2.getCar();
		model = object2.getModel();
		color = object2.getColor();
		licenseNum = object2.getLicenseNum();
		parkingTime = object2.getCarParked();
	}
	
	//– To report the amount of the fine, which is $25 for the first hour or part of an hour
	//that the car is illegally parked, plus $10 for every additional hour or part of an
	//hour that the car is illegally parked
	public int calcFine()
	{
		int fine = 25;
		fine = fine + (10 * parkingTime);
		return fine;
	}
	
	public String reportPolice()
	{
		
	}
	
	public String toString()
	{
		return "Make: " + carMake + "\nModel: " + model + "\nColor: " + color + "\nLicense Number: " + licenseNum + "Parking Time: " + parkingTime;
	}
}
