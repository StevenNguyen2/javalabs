package ex10;

public class ParkingMeter {
	//instance fields
	private int parkingTime;
	
	//constructor
	public ParkingMeter(int parkingTime)
	{
		this.parkingTime = parkingTime;
	}
	
	//accessor
	public int getParkingTime()
	{
		return parkingTime;
	}
}
