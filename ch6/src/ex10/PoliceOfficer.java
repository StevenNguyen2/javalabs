package ex10;

public class PoliceOfficer {
	//instance fields
	private String name;
	private int badgeNum;
	
	//constructor
	public PoliceOfficer(String name, int badgeNum)
	{
		this.name = name;
		this.badgeNum = badgeNum;
	}
	
	//accessor
	public String getName()
	{
		return name;
	}
	
	public int getBadgeNum()
	{
		return badgeNum;
	}
	
	//instantiate an object
	ParkedCar carObject = new ParkedCar("Lexus", "IS350", "Grey", 12345, 2);
	ParkingMeter meterObject = new ParkingMeter(3);
	
	public boolean issueTicket()
	{
		if (carObject.getCarParked() > meterObject.getParkingTime())
		{
			System.out.println("Your time has expired");
			ParkingTicket ticket = new ParkingTicket(carObject.getCarParked());
		}
	}
}


