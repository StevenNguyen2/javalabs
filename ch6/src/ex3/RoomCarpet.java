package ex3;

public class RoomCarpet {
	//instance fields
	private RoomDimension size;
	private double cost;
	
	//2 param constructor
	public RoomCarpet(RoomDimension dim, double cost)
	{
		this.size = dim;
		this.cost = cost;
	}
	
	public double getTotalCost()
	{
		return size.getArea() * cost;
	}
	
	public String toString()
	{
		return size.toString() + "\nThe cost per sq ft is " + cost + "\nThe total cost for carpet in this room is " + getTotalCost();
	}
}
