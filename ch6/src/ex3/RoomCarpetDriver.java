package ex3;
import java.util.Scanner;

public class RoomCarpetDriver {

	public static void main(String[] args) {
		//variables
		double widthInput;
		double lengthInput;
		double costInput;
		
		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user
		System.out.println("What is the width of the room?");
		widthInput = keyboard.nextDouble();
		
		//prompt user
		System.out.println("What is the length of the room?");
		lengthInput = keyboard.nextDouble();
			
		RoomDimension rd = new RoomDimension(lengthInput, widthInput);
		
		//prompt user
		System.out.println("What is the cost of the room?");
		costInput = keyboard.nextDouble();
		
		RoomCarpet rc = new RoomCarpet(rd, costInput);
		
		System.out.println(rc.toString());
	}

}
