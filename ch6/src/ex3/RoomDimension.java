package ex3;

public class RoomDimension {
	//instance fields
	private double length;
	private double width;
	
	//2 param constructor
	public RoomDimension(double length, double width)
	{
		this.length = length;
		this.width = width;
	}
	
	public double getArea()
	{
		return length * width;
	}
	
	public String toString()
	{
		return "\nThe width is " + width + "\nThe length is " + length;
	}
}
