package ex5;
import java.util.Scanner;

public class MonthDriver {

	public static void main(String[] args) {
		//variables
		int month1;
		int month2;

		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user
		System.out.println("Enter month number");
		month1 = keyboard.nextInt();
		
		System.out.println("Enter month number");
		month2 = keyboard.nextInt();
		
		//instantiate an object set attributes		
		Month object = new Month(month1);
		Month object2 = new Month(month2);
		
		//output
		System.out.println("Month 1: " + object.getMonthName());
		System.out.println("Month 2: " + object2.getMonthName());
		//greater than method
		System.out.println("Is month 1 greater than month 2? " + object.greaterThan(object2));
		//less than method
		System.out.println("Is month 1 less than month 2? " + object.lessThan(object2));
		//equals method
		System.out.println("Are the months equal? " + object.equals(object2));
		//toString method
		System.out.println(object.toString());
		System.out.println(object2.toString());
		
	}

}
