package ex5;

public class Month 
{
	//instance fields
	//enum monthNumber {JANUARY, FEBRUARY, MARCH, APRIL, MAY, JUNE, JULY, AUGUST, SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER};
	private int monthNumber;
	
	//no arg (default) constructor
	public Month()
	{
		monthNumber = 1;
	}
	
	//constructor that accepts the number of the month as an argument. It should set the
	//monthNumber field to the value passed as the argument. If a value less than 1 or greater
	//than 12 is passed, the constructor should set monthNumber to 1.
	public Month(int monthNumber)
	{
		this.monthNumber = monthNumber;
		if (monthNumber < 1 || monthNumber > 12)
		{
			monthNumber = 1;
		}
	}
	
	public Month(String name)
	{		
		switch (name)
		{
		case "January":
			monthNumber = 1;
			break;
		case "February":
			monthNumber = 2;
			break;
		case "March":
			monthNumber = 3;
			break;
		case "April":
			monthNumber = 4;
			break;
		case "May":
			monthNumber = 5;
			break;
		case "June":
			monthNumber = 6;
			break;
		case "July":
			monthNumber = 7;
			break;
		case "August":
			monthNumber = 8;
			break;
		case "September":
			monthNumber = 9;
			break;
		case "October":
			monthNumber = 10;
			break;
		case "November":
			monthNumber = 11;
			break;
		case "December":
			monthNumber = 12;
			break;
		}
	}
	
	//A setMonthNumber method that accepts an int argument, which is assigned to the monthNumber field. 
	//If a value less than 1 or greater than 12 is passed, the method should set monthNumber to 1.
	public void setMonthNumber(int monthNumber)
	{
		if (monthNumber < 1 || monthNumber > 12)
		{
			monthNumber = 1;
		}

	}
			
	//A getMonthNumber method that returns the value in the monthNumber field.
	public int getMonthNumber()
	{
		return monthNumber;
	}
	
	//A getMonthName method that returns the name of the month. For example, if the
	//monthNumber field contains 1, then this method should return “January”.
	public String getMonthName()
	{
		String name = "";
		if (monthNumber == 1)
		{
			name = "January";
		}
		else if (monthNumber == 2)
		{
			name = "February";
		}
		else if (monthNumber == 3)
		{
			name = "March";
		}
		else if (monthNumber == 4)
		{
			name = "April";
		}
		else if (monthNumber == 5)
		{
			name = "May";
		}
		else if (monthNumber == 6)
		{
			name = "June";
		}
		else if (monthNumber == 7)
		{
			name = "July";
		}
		else if (monthNumber == 8)
		{
			name = "August";
		}
		else if (monthNumber == 9)
		{
			name = "September";
		}
		else if (monthNumber == 10)
		{
			name = "October";
		}
		else if (monthNumber == 11)
		{
			name = "November";
		}
		else if (monthNumber == 12)
		{
			name = "December";
		}
		return name;
	}
	
	//A toString method that returns the same value as the getMonthName method.
	public String toString()
	{
		return "Month Name: " + getMonthName();
	}
	
	//An equals method that accepts a Month object as an argument. If the argument object
	//holds the same data as the calling object, this method should return true. Otherwise,
	//it should return false.
	public boolean equals(Month object2)
	{
		if(getMonthName() == object2.getMonthName())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	//A greaterThan method that accepts a Month object as an argument. If the calling
	//object’s monthNumber field is greater than the argument’s monthNumber field, this
	//method should return true. Otherwise, it should return false.
	public boolean greaterThan(Month object)
	{
		if(monthNumber > object.monthNumber)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	//A lessThan method that accepts a Month object as an argument. If the calling object’s
	//monthNumber field is less than the argument’s monthNumber field, this method should return true. Otherwise, it should return false.
	public boolean lessThan(Month object)
	{
		if(monthNumber < object.monthNumber)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
