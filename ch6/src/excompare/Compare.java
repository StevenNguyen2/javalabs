package excompare;

public class Compare {
	public static void main(String[] args)
	{
		String lexus = "Lexus";
		String bmw = "BMW";
		String lexus2 = lexus;
		
		if (lexus.equals(bmw))
		{
			System.out.println("Lexus can't be compared to a BMW");
		}
		if (lexus.equals(lexus2))
		{
			System.out.println("Lexus can be compared to a Lexus");
		}
	}
}
