package ex1;
import java.util.Scanner;

public class RainFallDriver {

	public static void main(String[] args) {
		//variables
		double input1 = 0;
		double[] totalRainFall = new double[12];
		
		Rainfall rain = new Rainfall(totalRainFall);

		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user
		/*
		while (input1 <= 0) //don't accept negative numbers
		{
			for (int i = 1; i <= totalRainFall.length; i++)
			{
				System.out.println("Enter total rainfall for month " + i);
				totalRainFall[i] = keyboard.nextDouble();
				//totalRainFall[i] = input1;
			}
		}

		*/
		
		for (int i = 0; i < totalRainFall.length; i++)
		{
			do
			{
				System.out.println("Enter total rainfall for month " + (i + 1));
				totalRainFall[i] = keyboard.nextDouble();
			}
			while(totalRainFall[i] < 0);
		}
		
		
		//output
		System.out.println("Total: " + rain.calcRainForYear(totalRainFall));
		System.out.println("Average: " + rain.calcAverageMonthlyRain(totalRainFall));
		System.out.println("Most Rain: " + rain.calcMostRain(totalRainFall));
		System.out.println("Least Rain: " + rain.calcLeastRain(totalRainFall));
	}

}
