package ex2;

public class Payroll {
	//instance fields
	private int[] employeeId = { 5658845, 4520125, 7895122, 8777541, 8451277, 1302850, 7580489 };
	private int[] hours = new int[7];
	private double[] payRate = new double[7];
	private double[] wages = new double[7];
	
	//relating data in each array through the subscripts
	//mutator
	public void setHours(int[] hours)
	{
		for (int i = 0; i < employeeId.length; i++)
		{
			this.hours[i] = hours[i];
		}
		setWages();
	}
	
	public void setPayRate(double[] payRate)
	{
		for (int i = 0; i < employeeId.length; i++)
		{
			this.payRate[i] = payRate[i];
		}
		setWages();
	}
	
	public void setWages()
	{
		for (int i = 0; i < employeeId.length; i++)
		{
			wages[i] = payRate[i] * hours[i];
		}
	}
	
	//accessor
	public int getEmployeeId(int i)
	{
		return employeeId[i];
	}
	
	public int getHours(int i)
	{
		return hours[i];
	}
	
	public double getPayRate(int i)
	{
		return payRate[i];
	}
	
	public double getWages(int i)
	{
		return wages[i];
	}
	
	//method to return gross pay
	public double returnGrossPay(int employeeIdd)
	{
		double totalWages = 0;
		
		for(int i = 0; i < employeeId.length; i++)
		{
			//wages[i] = hours[i] * payRate[i];
			totalWages = this.wages[i];
		}
		//return totalWages;
		return totalWages;
	}
	
	//toString method
	public String toString()
	{
		String total = "";
		
		for (int i = 0; i < employeeId.length; i++)
		{
			total = "Employee #: " + employeeId[i] + "\nGross Pay: " + wages[i];
		}
		return total;
	}
}
