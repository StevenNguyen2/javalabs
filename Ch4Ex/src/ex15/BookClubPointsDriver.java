package ex15;
import java.util.Scanner;

public class BookClubPointsDriver {

	public static void main(String[] args) {
		//variables
		int books;
		
		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user
		System.out.println("Enter books purchased: ");
		books = keyboard.nextInt();
		
		//instantiate an object
		BookClubPoints b1 = new BookClubPoints();
		
		//set attributes
		b1.setBooks(books);
		
		//output
		System.out.println("Books: " + books);
		System.out.println("Points: " + b1.getPoints());
	}

}
