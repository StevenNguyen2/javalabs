package ex15;

public class BookClubPoints {
	//instance fields
	private int books;
	
	//mutator
	public void setBooks(int books)
	{
		this.books = books;
	}
	
	//accessor
	public int getBooks()
	{
		return books;
	}
	
	//method to return points awarded
	public int getPoints()
	{
		int points;
		
		if (books <= 0)
		{
			points = 0;
		}
		else if (books == 1)
		{
			points = 5;
		}
		else if (books == 2)
		{
			points = 15;
		}
		else if (books == 3)
		{
			points = 30;
		}
		else
		{
			points = 60;
		}
		
		return points;
	}
}
