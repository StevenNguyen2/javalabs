package ex6;
import java.util.Scanner;

public class ShippingChargesDriver {

	public static void main(String[] args) {
		//variables
		double userInput;
		double userInput2;
		
		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user
		System.out.println("What's the weight of your package?");
		userInput = keyboard.nextDouble();
		
		System.out.println("How many miles?");
		userInput2 = keyboard.nextDouble();
		
		//instantiate an object
		ShippingCharges s1 = new ShippingCharges(userInput, userInput2);
		
		//output
		System.out.printf("Total: $%,.2f\n", s1.returnShippingCharges());
	}

}
