package ex6;

public class ShippingCharges {
	//instance fields
	private double weight;
	private double shippingMiles;
	
	//Constructor
	public ShippingCharges(double weight, double shippingMiles)
	{
		this.weight = weight;
		this.shippingMiles = shippingMiles;
	}

	//method that returns the shipping charges
	public double returnShippingCharges()
	{
		double shippingCharges = 0;
		double counter = 0;
		
		//works w/o accessor
		
		if (weight <= 2)
		{
			shippingCharges = 1.10;
		}
		if (weight > 2 && weight <= 6)
		{
			shippingCharges = 2.20;
		}
		if (weight > 6 && weight <= 10)
		{
			shippingCharges = 3.70;
		}
		if (weight > 10)
		{
			shippingCharges = 4.80;
		}
		
		counter = (int)shippingMiles / 500;
		
		if (shippingMiles % 500 != 0)
		{
			
			counter++;
		}
		
		return shippingCharges * counter;
	}
}
