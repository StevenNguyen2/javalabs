package ex3;

public class TestScores {
	//instance fields
	private double testScore1;
	private double testScore2;
	private double testScore3;
	private double avg;
	private String letterGrade;

	//constructor
	public TestScores(double testScore1, double testScore2, double testScore3)
	{
		this.testScore1 = testScore1;
		this.testScore2 = testScore2;
		this.testScore3 = testScore3;
	}
	
	//accessors
	public double getScore1()
	{
		return testScore1;
	}
	
	public double getScore2()
	{
		return testScore2;
	}
	
	public double getScore3()
	{
		return testScore3;
	}
	
	//method that returns average of the test scores
	public double calcAvg()
	{
		avg = (testScore1 + testScore2 + testScore3) / 3;
		return avg;
	}
	
	//method that returns the letter grade
	public String calcLetterGrade()
	{
		if (avg <= 100 && avg >= 90)
		{
			letterGrade = "A";
			System.out.println(letterGrade);
		}
		else if (avg <= 89 && avg >= 80)
		{
			letterGrade = "B";
			System.out.println(letterGrade);
		}
		else if (avg <= 79 && avg >= 70)
		{
			letterGrade = "C";
			System.out.println(letterGrade);
		}
		else if (avg <= 69 && avg >= 60)
		{
			letterGrade = "D";
			System.out.println(letterGrade);
		}
		else if (avg <= 60)
		{
			letterGrade = "F";
			System.out.println(letterGrade);
		}
		return letterGrade;
	}
}
