package ex3;
import java.util.Scanner;

public class TestScoresDriver {

	public static void main(String[] args) {
		//variables
		double testScore1;
		double testScore2;
		double testScore3;
		
		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user
		System.out.println("Enter a test score");
		testScore1 = keyboard.nextDouble();
		
		System.out.println("Enter a 2nd test score");
		testScore2 = keyboard.nextDouble();
		
		System.out.println("Enter a 3rd test score");
		testScore3 = keyboard.nextDouble();
		
		TestScores t1 = new TestScores(testScore1, testScore2, testScore3);
		
		//output
		System.out.println("Average: " + t1.calcAvg());
		System.out.println("Letter Grade: " + t1.calcLetterGrade());
	}

}
