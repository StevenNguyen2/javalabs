package ex9;
import java.util.Scanner;

public class SpeedOfSoundDriver {

	public static void main(String[] args) {
		//variables
		String userInput;
		double distanceInput;
		
		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user
		System.out.println("Select air, water, or steel");
		userInput = keyboard.nextLine();
		
		System.out.println("Enter a distance");
		distanceInput = keyboard.nextInt();
		
		//instantiate an object
		SpeedOfSound s1 = new SpeedOfSound();
		
		//set attributes
		s1.setDistance(distanceInput);
		
		//output
		if(userInput.equalsIgnoreCase("air")) 
		{
			System.out.println(s1.getSpeedInAir() + " seconds");
		}
		else if (userInput.equalsIgnoreCase("water"))
		{
			System.out.println(s1.getSpeedInWater() + " seconds");
		}
		else if (userInput.equalsIgnoreCase("steel"))
		{ 
			System.out.println(s1.getSpeedInSteel() + " seconds");
		}
		
	}

}
