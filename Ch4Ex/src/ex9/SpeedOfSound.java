package ex9;

public class SpeedOfSound {
	//instance fields
	private double distance;
	private double time;
	
	//mutator
	public void setDistance(double distance)
	{
		this.distance = distance;
	}
	
	//accessor
	public double getDistance()
	{
		return distance;
	}
	
	//method to get speed of soundwave to travel in air
	public double getSpeedInAir()
	{
		time = distance/1100;
		return time;
	}
	
	
	//method to return number of seconds it would take a sound wave to travel in water
	public double getSpeedInWater()
	{
		time = distance/4900;
		return time;
	}
	
	//method to return number of seconds it would take a sound wave to travel in steel
	public double getSpeedInSteel()
	{
		time = distance/16400;
		return time;
	}
}
