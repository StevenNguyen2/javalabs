package ex5;
import java.util.Scanner;

public class BankChargesDriver {

	public static void main(String[] args) {
		//instantiate an object
		BankCharges b1 = new BankCharges();
		
		//variables
		double userInput;
		double balanceInput;
		
		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user
		System.out.println("Enter checks written");
		userInput = keyboard.nextDouble();
		
		System.out.println("Enter balance");
		balanceInput = keyboard.nextDouble();
		
		//set attributes
		b1.setBalance(balanceInput);
		b1.setChecks(userInput);
		
		//output
		System.out.printf("Checks Written: " + b1.getChecks());
		System.out.println("");
		System.out.printf("Ending Balance: $%,.2f\n", b1.getBalance() - b1.calcBankFees() - 10);
	}

}
