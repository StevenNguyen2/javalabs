package ex5;

public class BankCharges {
	//instance fields
	private double endingBalance;
	private double checksWritten;
	private double total;
	
	
	//mutator
	public void setBalance(double endingBalance)
	{
		this.endingBalance = endingBalance;
	}
	
	public void setChecks(double checksWritten)
	{
		this.checksWritten = checksWritten;
	}
	
	//accessor
	public double getBalance()
	{
		return endingBalance;
	}
	
	public double getChecks()
	{
		return checksWritten;
	}
	
	//method that returns the bank's service fees
	public double calcBankFees()
	{
		if (checksWritten <= 20)
		{
			total = checksWritten * .1;
		}
		else if (checksWritten >= 20 && checksWritten <= 39)
		{
			total = checksWritten * .08;
		}
		else if (checksWritten >= 40 && checksWritten >= 59)
		{
			total = checksWritten * .06;
		}
		else if (checksWritten >= 60.0)
		{
			total = checksWritten * .04;
		}
		
		if (endingBalance < 400)
		{
			total += 15;;
		}
		
		return total;
	}
}
