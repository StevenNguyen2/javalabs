package ex1;
import java.util.Scanner;

public class RomanNumerals {
	public static void main(String[] args)
	{
		//variables
		int userInput;
		
		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user
		System.out.println("Enter a number within 1-10");
		userInput = keyboard.nextInt();
		
		//output
		if (userInput == 1)
		{
			System.out.println("I");
		}
		else if (userInput == 2)
		{
			System.out.println("II");
		}
		else if (userInput == 3) 
		{
			System.out.println("III");
		}
		else if (userInput == 4)
		{
			System.out.println("IV");
		}
		else if (userInput == 5)
		{
			System.out.println("V");
		}
		else if (userInput == 6)
		{
			System.out.println("VI");
		}
		else if (userInput == 7)
		{
			System.out.println("VII");
		}
		else if (userInput == 8 )
		{
			System.out.println("VIII");
		}
		else if (userInput == 9)
		{
			System.out.println("IX");
		}
		else if (userInput == 10)
		{
			System.out.println("X");
		}
	}
}
