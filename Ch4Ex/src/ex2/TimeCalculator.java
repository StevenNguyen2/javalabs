package ex2;
import java.util.Scanner;

public class TimeCalculator {

	public static void main(String[] args) {
		//variables
		double hours;
		double minutes;
		double days;
		double userInput;
		
		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user
		System.out.println("Enter a number of seconds");
		userInput = keyboard.nextDouble(); 
		
		//output
		if (userInput >= 60)
		{
			minutes = userInput / 60;
			System.out.println(minutes + " minute(s)");
		}
		else if (userInput >= 3600)
		{
			hours = userInput / 3600;
			System.out.println(hours + " hour(s)");
		}
		else if (userInput >= 86400)
		{
			days = userInput / 86400;
			System.out.println(days + " days(s)");
		}
	}

}
