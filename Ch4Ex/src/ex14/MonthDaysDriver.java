package ex14;
import java.util.Scanner;

public class MonthDaysDriver {

	public static void main(String[] args) {
		//variables
		int monthInput;
		int yearInput;
		
		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user
		System.out.println("Enter a month (1-12):");
		monthInput = keyboard.nextInt();
		
		System.out.println("Enter a year:");
		yearInput = keyboard.nextInt();
		
		//instantiate an object and set attributes
		MonthDays md1 = new MonthDays(monthInput, yearInput);
		
		//output
		System.out.println(md1.getNumberOfDays() + " days");
	}

}
