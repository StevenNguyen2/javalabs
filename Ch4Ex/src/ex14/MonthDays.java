package ex14;

public class MonthDays {
	//instance fields
	private int month;
	private int year;
	
	//constructor w/ 2 parameters
	public MonthDays(int month, int year)
	{
		this.month = month;
		this.year = year;
	}
	
	//accessor
	public int getMonth()
	{
		return month;
	}
	
	public int getYear()
	{
		return year;
	}
	
	//method that returns the number of days in the specified month
	public int getNumberOfDays()
	{
		int numberOfDays = 0;
		
		switch(month)
		{
			case 1:
				numberOfDays = 31;
				break;
			case 2:
				//February has 29 years during leap year
				if (year % 100 == 0)
				{
					if (year % 400 == 0)
					{
						numberOfDays = 29;
					}
				}
				else if (year % 4 == 0)
				{
					numberOfDays = 29;
				}
				else
				{
					numberOfDays = 28;
				}
				break;
			case 3:
				numberOfDays = 31;
				break;
			case 4:
				numberOfDays = 30;
				break;
			case 5:
				numberOfDays = 31;
				break;
			case 6:
				numberOfDays = 30;
				break;
			case 7:
				numberOfDays = 31;
				break;
			case 8:
				numberOfDays = 31;
				break;
			case 9:
				numberOfDays = 30;
				break;
			case 10:
				numberOfDays = 31;
				break;
			case 11:
				numberOfDays = 30;
				break;
			case 12:
				numberOfDays = 31;
				break;	
		}
		return numberOfDays;
	}
}
