package ex11;

public class MobileServiceProvider {
	//instance fields
		private String packageLetter;
		private double minutesUsed;
		
		//mutator
		public void setPackage(String packageLetter)
		{
			this.packageLetter = packageLetter;
		}
		
		public void setMinutes(double minutesUsed)
		{
			this.minutesUsed = minutesUsed;
		}
		
		//accessor
		public String getPackage()
		{
			return packageLetter;
		}
		
		public double getMinutes()
		{
			return minutesUsed;
		}
		
		//method that returns the total charges
		public double calcTotal()
		{
			double total = 0;
			
			if (packageLetter.equalsIgnoreCase("A"))
			{
				if (minutesUsed <= 450)
				{
					total = 39.99;
				}
				else
				{
					total = (minutesUsed - 450) * .45 + 39.99;
				}
			}
			else if (packageLetter.equalsIgnoreCase("B"))
			{
				if (minutesUsed <= 900)
				{
					total = 59.99;
				}
				else
				{
					total = (minutesUsed - 900) * .40 + 59.99;
				}
			}
			else if (packageLetter.equalsIgnoreCase("C"))
			{
				total = 69.99;
			}
			
			return total;
		}
}
