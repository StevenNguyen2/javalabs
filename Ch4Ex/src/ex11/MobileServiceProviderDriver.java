package ex11;
import java.util.Scanner;

public class MobileServiceProviderDriver {

	public static void main(String[] args) {
		//variables
		String userInput;
		double minutesInput;
		
		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user
		System.out.println("Select a package (A, B, or C)");
		userInput = keyboard.nextLine();
		
		System.out.println("Enter number of minutes used");
		minutesInput = keyboard.nextDouble();
		
		//instantiate an object
		MobileServiceProvider m1 = new MobileServiceProvider();
		
		//set attributes
		m1.setPackage(userInput);
		m1.setMinutes(minutesInput);

		//output
		System.out.printf("Total: $%,.2f\n", m1.calcTotal());
	}

}
