package ex7;
import java.util.Scanner;

public class FatGramsDriver {

	public static void main(String[] args) {
		//variables
		double fatInput;
		double caloriesInput;
		
		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user
		System.out.println("Enter the number of calories");
		caloriesInput = keyboard.nextDouble();
		
		System.out.println("Enter the number of fat grams");
		fatInput = keyboard.nextDouble();
		
		//instantiate an object and set attributes
		FatGram f1 = new FatGram(caloriesInput, fatInput);
		
		//output
		System.out.printf("%.2f%%", f1.returnPercentage());
	}

}
