package ex7;

public class FatGram {
	//instance fields
	private double calories;
	private double fatGrams;
	
	//constructor
	public FatGram(double calories, double fatGrams)
	{
		this.calories = calories;
		this.fatGrams = fatGrams;
	}
	
	//accessor
	public double getCalories()
	{
		return calories;
	}
	
	public double getFatGrams()
	{
		return fatGrams;
	}
	
	//method that returns the percentage of calories that come from fat
	public double returnPercentage()
	{
		double caloriesFromFat;
		double percentOfCalories;
		
		caloriesFromFat = fatGrams * 9;
		percentOfCalories = caloriesFromFat / calories;
		
		if (caloriesFromFat < (calories * .3))
		{
			System.out.println("Food is low in fat");
		}
		else if (caloriesFromFat > calories)
		{
			System.out.println("Error: Numbers are invalid");
		}
		
		return percentOfCalories * 100;
	}
}
