package ex16;

public class MagicDates {
	//instance fields
	private int month;
	private int day;
	private int year;
	
	//constructor
	public MagicDates(int month, int day, int year)
	{
		this.month = month;
		this.day = day;
		this.year = year;
	}
	
	//accessor
	public int getMonth()
	{
		return month;
	}
	
	public int getDay()
	{
		return day;
	}
	
	public int getYear()
	{
		return year;
	}
	
	//method that returns true if date passed to constructor is magic
	public boolean isMagic()
	{
		boolean magicNum;
		
		if (month * day == year)
		{
			magicNum = true;
		}
		else
		{
			magicNum = false;
		}
		return magicNum;
	}
}
