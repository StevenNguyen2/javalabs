package ex16;
import java.util.Scanner;

public class MagicDatesDriver {

	public static void main(String[] args) {
		//variables
		int month;
		int day;
		int year;
		
		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user
		System.out.println("Enter month");
		month = keyboard.nextInt();
		
		System.out.println("Enter day");
		day = keyboard.nextInt();
		
		System.out.println("Enter year (as two digit integer ex: 60)");
		year = keyboard.nextInt();
		
		//instantiate an object and set attributes
		MagicDates magic = new MagicDates(month, day, year);
		
		//output
		if (month * day == year) 
		{
			System.out.println(magic.getMonth() + "/" + magic.getDay() + "/" + magic.getYear() + " is magic");
		}
		else
		{
			System.out.println("");
		}
	}

}
