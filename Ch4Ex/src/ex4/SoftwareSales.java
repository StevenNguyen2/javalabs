package ex4;

public class SoftwareSales {
	//instance fields
	private double unitsSold;
	private double total;
	
	//mutators
	public void setUnits(double unitsSold)
	{
		this.unitsSold = unitsSold;
	}
	
	//accessors
	public double getUnits()
	{
		return unitsSold;
	}
	
	//method that returns the total cost of purchase
	public double calcTotal()
	{
		if (unitsSold >= 10 && unitsSold <= 19)
		{
			total = (unitsSold * 99) * .2;
		}
		if (unitsSold >= 20 && unitsSold <= 49)
		{
			total = (unitsSold * 99) * .3;
		}
		if (unitsSold >= 50 && unitsSold <= 99)
		{
			total = (unitsSold * 99) * .4;
		}
		if (unitsSold >= 100)
		{
			total = (unitsSold * 99) * .5;
		}
		return total;
	}
}
