package ex4;
import java.util.Scanner;

public class SoftwareSalesDriver {

	public static void main(String[] args) {
		SoftwareSales s1 = new SoftwareSales();
		
		//variables
		double userInput;
		
		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user
		System.out.println("How many units sold?");
		userInput = keyboard.nextDouble();
		
		//set an object's attributes
		s1.setUnits(userInput);
		
		//output
		System.out.printf("Total: $%,.2f\n", s1.calcTotal());
	}

}
