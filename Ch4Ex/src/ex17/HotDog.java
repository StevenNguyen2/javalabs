package ex17;

public class HotDog {
	//instance fields
	private int amountOfHotDogs;
	private int people;
	
	//constructor
	public HotDog(int amountOfHotDogs, int people)
	{
		this.amountOfHotDogs = amountOfHotDogs;
		this.people = people;
	}
	
	//accessor
	public int getHotDog()
	{
		return amountOfHotDogs;
	}
	
	public int getPeople()
	{
		return people;
	}
	
	//method to calculate the number of packages of hot dogs and the number of packages of hot
	//dog buns needed for a cookout, with the minimum amount of leftovers.
	public int calcPackages()
	{
		int hotDogsNeeded = amountOfHotDogs * people;
		int dogsPackageTotal = 10;
		int bunsPackageTotal = 8;
		int hotDogPackages = hotDogsNeeded / dogsPackageTotal;
		int bunsPackages = hotDogsNeeded / bunsPackageTotal;
		int hotDogCounter = 0;
		hotDogCounter = amountOfHotDogs / dogsPackageTotal;
		
		if (hotDogsNeeded % 10 != 0) 
		{
			hotDogCounter++;
		}
	}
}
