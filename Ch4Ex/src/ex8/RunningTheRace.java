package ex8;

public class RunningTheRace {
	//instance fields
	private String runner1;
	private String runner2;
	private String runner3;
	private double time;
	private double time2;
	private double time3;
	
	//Constructor
	public RunningTheRace(String runner1, String runner2, String runner3, double time, double time2, double time3)
	{
		this.runner1 = runner1;
		this.runner2 = runner2;
		this.runner3 = runner3;
		this.time = time;
		this.time2 = time2;
		this.time3 = time3;
	}
	
	//accessor
	public String getRunner1()
	{
		return runner1;
	}
	
	public String getRunner2()
	{
		return runner2;
	}
	
	public String getRunner3()
	{
		return runner3;
	}
	
	public double getTime()
	{
		return time;
	}
	
	public double getTime2()
	{
		return time2;
	}
	
	public double getTime3()
	{
		return time3;
	}
	
	//method that returns the name of runner in 1st, 2nd, or 3rd place
	public String getFirstPlace()
	{
		String winner;
		if (time < time2 && time < time3)
		{
			winner = String.format("%s got first place", runner1);
		}
		else if (time2 < time && time2 < time3)
		{
			winner = String.format("%s got first place", runner2);
		}
		else
		{
			winner = String.format("%s got first place", runner3);
		}
		return winner;
	}
	
	public String getSecondPlace()
	{
		String secondPlace;
		if ((time < time2 && time > time3) || (time > time2 && time < time3)) 
		{
			secondPlace = String.format("%s got second place", runner1);
		}
		else if ((time2 < time && time2 > time3) || (time2 > time && time2 < time3))
		{
			secondPlace = String.format("%s got second place", runner2);
		}
		else
		{
			secondPlace = String.format("%s got second place", runner3);
		}
		return secondPlace;
	}
	
	public String getThirdPlace()
	{
		String thirdPlace;
		if (time > time2 && time > time3)
		{
			thirdPlace = String.format("%s got third place", runner1);
		}
		else if (time2 < time && time2 < time3)
		{
			thirdPlace = String.format("%s got third place", runner2);
		}
		else
		{
			thirdPlace = String.format("%s got third place", runner3);
		}
		return thirdPlace;
	}
}
