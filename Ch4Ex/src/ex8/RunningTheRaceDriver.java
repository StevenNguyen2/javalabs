package ex8;

public class RunningTheRaceDriver {

	public static void main(String[] args) {
		RunningTheRace r1 = new RunningTheRace("Steven", "Lyle", "Gudmestad", 3, 4, 1);
		
		System.out.println(r1.getFirstPlace());
		System.out.println(r1.getSecondPlace());
		System.out.println(r1.getThirdPlace());
	}

}
