package ex13;
import java.util.Scanner;

public class BodyMassIndexDriver {

	public static void main(String[] args) {
		//variables
		double weightInput;
		double heightInput;
		
		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user
		System.out.println("Enter weight (in pounds)");
		weightInput = keyboard.nextDouble();
		
		System.out.println("Enter height (in inches)");
		heightInput = keyboard.nextDouble();
		
		//instantiate an object
		BodyMassIndex bmi = new BodyMassIndex();
		
		//set attributes
		bmi.setWeight(weightInput);
		bmi.setHeight(heightInput);
		
		//output
		if (bmi.calcBMI() >= 18.5 && bmi.calcBMI() < 25)
		{
			System.out.printf("%.2f is considered to be optimal", bmi.calcBMI());
		}
		else if (bmi.calcBMI() < 18.5) 
		{
			System.out.printf("%.2f is considered to be underweight", bmi.calcBMI());
		}
		else if (bmi.calcBMI() > 25)
		{
			System.out.printf("%.2f is considered to be overweight", bmi.calcBMI());
		}

	}

}
