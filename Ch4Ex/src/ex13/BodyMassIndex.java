package ex13;

public class BodyMassIndex {
	//instance fields
	private double weight;
	private double height;
	
	//mutator
	public void setWeight(double weight)
	{
		this.weight = weight;
	}
	
	public void setHeight(double height)
	{
		this.height = height;
	}
	
	//accessor
	public double getWeight()
	{
		return weight;
	}
	
	public double getHeight()
	{
		return height;
	}
	
	//method to calculate BMI
	public double calcBMI()
	{
		double total = 0;
		
		total = weight * 703/(height * height);
		return total;
	}
}
