package ex10;

public class FreezeBoilPoints {
	//instance fields
	private int temp;
	private boolean checkTemp;
	
	//mutator
	public void setTemp(int temp)
	{
		this.temp = temp;
	}
	
	//accessor
	public int getTemp()
	{
		return temp;
	}
	
	//method to return boolean value true if temp is at or below freezing point of ethyl alcohol
	public boolean isEthylFreezing()
	{
		if (temp <= -173)
		{
			checkTemp = true;
		}
		else
		{
			checkTemp = false;
		}
		return checkTemp;
	}
	
	//method to return boolean value true if temp is at or above the boiling point of ethyl alcohol
	public boolean isEthylBoiling()
	{
		if(temp >= 172)
		{
			checkTemp = true;
		}
		else
		{
			checkTemp = false;
		}
		return checkTemp;
	}
	
	//method to return boolean value true if temp is at or below the freezing point of oxygen
	public boolean isOxygenFreezing()
	{
		if (temp <= -362)
		{
			checkTemp = true;
		}
		else
		{
			checkTemp = false;
		}
		return checkTemp;
	}
	
	//method to return boolean value true if temp is at or above the boiling point of oxygen
	public boolean isOxygenBoiling()
	{
		if (temp >= -306)
		{
			checkTemp = true;
		}
		else
		{
			checkTemp = false;
		}
		return checkTemp;
	}
	
	//method to return boolean value true if temp is at or below the freezing point of water
	public boolean isWaterFreezing()
	{
		if (temp <= 32)
		{
			checkTemp = true;
		}
		else
		{
			checkTemp = false;
		}
		return checkTemp;
	}
	
	//method to return boolean value true if temp is at or above the boiling point of water
	public boolean isWaterBoiling()
	{
		if (temp >= 212)
		{
			checkTemp = true;
		}
		else
		{
			checkTemp = false;
		}
		return checkTemp;
	}
}
