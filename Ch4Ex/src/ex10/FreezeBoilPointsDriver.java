package ex10;
import java.util.Scanner;

public class FreezeBoilPointsDriver {

	public static void main(String[] args) {
		//variables
		int userInput;
		
		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user
		System.out.println("Enter a temperature");
		userInput = keyboard.nextInt();
		
		//instantiate an object
		FreezeBoilPoints f1 = new FreezeBoilPoints();
		
		//set attributes
		f1.setTemp(userInput);
		
		//output
		
		if (f1.isEthylFreezing() == true)
		{
			System.out.println("Ethyl will freeze");
		}
		else if (f1.isEthylBoiling() == true)
		{
			System.out.println("Ethyl will boil");
		}
		
		if (f1.isOxygenFreezing() == true)
		{
			System.out.println(" Oxygen will freeze");
		}
		else if (f1.isOxygenBoiling() == true)
		{
			System.out.println("Oxygen will boil");
		}
		
		if (f1.isWaterFreezing() == true)
		{
			System.out.println("Water will freeze");
		}
		else if (f1.isWaterBoiling() == true)
		{
			System.out.println("Water will boil");
		}
	}

}
