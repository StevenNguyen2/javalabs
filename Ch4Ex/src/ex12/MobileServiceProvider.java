package ex12;

public class MobileServiceProvider {
	//instance fields
	private String packageLetter;
	private double minutesUsed;
	
	//mutator
	public void setPackage(String packageLetter)
	{
		this.packageLetter = packageLetter;
	}
	
	public void setMinutes(double minutesUsed)
	{
		this.minutesUsed = minutesUsed;
	}
	
	//accessor
	public String getPackage()
	{
		return packageLetter;
	}
	
	public double getMinutes()
	{
		return minutesUsed;
	}
	
	//method that returns the total of each Package
	public double calcPackageA()
	{
		double aTotal = 0;
		
		if (minutesUsed <= 450)
		{
			aTotal = 39.99;
		}
		else
		{
			aTotal = (minutesUsed - 450) * .45 + 39.99;
		}
		return aTotal;
	}
	
	public double calcPackageB()
	{
		double bTotal = 0;
		
		if (minutesUsed <= 900)
		{
			bTotal = 59.99;
		}
		else
		{
			bTotal = (minutesUsed - 900) * .40 + 59.99;
		}
		return bTotal;
	}
	
	public double calcPackageC()
	{
		double cTotal = 0;
		
		cTotal = 69.99;
		return cTotal;
	}	
}
