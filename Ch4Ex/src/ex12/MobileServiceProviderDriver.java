package ex12;

import java.util.Scanner;

public class MobileServiceProviderDriver {
	public static void main(String[] args) {
		//variables
		String userInput;
		double minutesInput;
		
		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user
		System.out.println("Select a package (A, B, or C)");
		userInput = keyboard.nextLine();
		
		System.out.println("Enter number of minutes used");
		minutesInput = keyboard.nextDouble();
		
		//instantiate an object
		MobileServiceProvider m1 = new MobileServiceProvider();
		
		//set attributes
		m1.setPackage(userInput);
		m1.setMinutes(minutesInput);
		
		//output
		if (userInput.equalsIgnoreCase("A")) 
		{
			System.out.printf("Total: $%,.2f\n", m1.calcPackageA());
			if (m1.calcPackageA() > m1.calcPackageB())
			{
				System.out.printf("Opting for Package B, you could save $%,.2f\n", (m1.calcPackageA() - m1.calcPackageB()));
			}
			if (m1.calcPackageA() > m1.calcPackageC())
			{
				System.out.printf("Opting for Package C, you could save $%,.2f\n", (m1.calcPackageA() - m1.calcPackageC()));
			}
		}	
		if (userInput.equalsIgnoreCase("B")) 
		{
			System.out.printf("Total: $%,.2f\n", m1.calcPackageB());
			if (m1.calcPackageB() > m1.calcPackageA()) 
			{
				System.out.printf("Opting for Package A, you could save $%,.2f\n", (m1.calcPackageB() - m1.calcPackageA()));
			}
			if (m1.calcPackageB() > m1.calcPackageC())
			{
				System.out.printf("Opting for Package C, you could save $%,.2f\n", (m1.calcPackageB() - m1.calcPackageC()));
			}
		}	
		if (userInput.equalsIgnoreCase("C")) 
		{
			System.out.printf("Total: $%,.2f\n", m1.calcPackageC());
		}	
	}
}
