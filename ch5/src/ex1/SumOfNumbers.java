package ex1;

public class SumOfNumbers 
{
	//instance fields
	private int integer;
	int total = 0;
	
	//mutator
	public void setInteger(int integer)
	{
		this.integer = integer;
	}
	
	//accessor
	public int getInteger()
	{
		return integer;
	}

	//method to get sum of all integers from 1 to number entered
	public int calcSum() 
	{
		if (integer > 0) 
		{
			for (int i = 1; i <= integer; i++)
			{
				total += i;
			}
		}
		else
		{
			System.out.println("Error: Enter a positive nonzero integer value");
		}
		return total;
	}
}
