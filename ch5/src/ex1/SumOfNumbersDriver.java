package ex1;
import java.util.Scanner;
import java.io.*;

public class SumOfNumbersDriver {

	public static void main(String[] args) throws IOException {
		PrintWriter sales = new PrintWriter("WeeklySales.txt");
		//variables
		int userInput;
		
		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user
		System.out.println("Please enter a positive nonzero integer value");
		userInput = keyboard.nextInt();
		
		//instantiate object
		SumOfNumbers sum1 = new SumOfNumbers();
		sum1.setInteger(userInput);
		
		//output
		System.out.println("Output: " + sum1.calcSum());
	}

}
