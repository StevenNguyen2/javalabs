package ex5;
import java.util.Scanner;
import java.text.DecimalFormat;

public class HotelOccupancy {
	public static void main(String[] args)
	{
		//variables
		int numFloors = 0;
		int roomsOnFloor = 0;
		int roomsOccupied = 0;
		int roomsOnFloorTotal = 0;
		int roomsOccupiedTotal = 0;
		
		
		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//create a DecimalFormat object
		DecimalFormat formatter = new DecimalFormat("#0.00");
		
		//prompt user
		while (numFloors < 1)
		{
			System.out.println("How many floors does the hotel have?");
			numFloors = keyboard.nextInt();
		}	
		
		//for loop
		for (int floors = 1; floors <= numFloors; floors++)
		{    
			roomsOnFloor = 0;
			while (roomsOnFloor < 10)
			{
				System.out.println("How many rooms on floor " + floors + " (please enter 10 or greater)");
				roomsOnFloor = keyboard.nextInt();
				roomsOnFloorTotal += roomsOnFloor;
			}
			
			System.out.println("How many rooms are occupied on floor " + floors);
			roomsOccupied = keyboard.nextInt();
			roomsOccupiedTotal += roomsOccupied;
		}
		
		double occupancy = (double)roomsOccupiedTotal/roomsOnFloorTotal;
		
		System.out.println("# of rooms: " + roomsOnFloorTotal);
		System.out.println("# of rooms occupied: " + roomsOccupiedTotal);
		System.out.println("# of rooms vacant: " + (roomsOnFloorTotal - roomsOccupiedTotal));
		System.out.println("Occupancy rate: " + formatter.format(occupancy));
		
	}
}
