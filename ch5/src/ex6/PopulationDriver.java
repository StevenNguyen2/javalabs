package ex6;
import java.util.Scanner;

public class PopulationDriver {

	public static void main(String[] args) {
		//variables
		int startingInput = 0;
		int daysInput = 0;
		double avgIncreaseInput = -1;
		
		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
				
		//prompt user
		while (startingInput < 2) 
		{
			System.out.println("Enter starting size of the population");
			startingInput = keyboard.nextInt();
		}
		
		while (avgIncreaseInput < 0)
		{
			System.out.println("Enter average daily increase (as decimal)");
			avgIncreaseInput = keyboard.nextDouble();
		}
		
		while (daysInput < 1)
		{
			System.out.println("Enter number of days to multiply");
			daysInput = keyboard.nextInt();
		}
		
		//instantiate an object
		Population p1 = new Population();
		p1.setOrganisms(startingInput);
		p1.setDays(daysInput);
		p1.setPopulation(avgIncreaseInput);
		
		//output
		System.out.println("Total Population: " + p1.displayPopulation());
	}

}
