package ex6;

public class Population 
{
	//instance fields
	private int startingNumOrganisms;
	private double avgDailyPopulation; //as a percentage
	private int daysMultiplied;
	
	//mutator
	public void setOrganisms(int startingNumOrganisms)
	{
		this.startingNumOrganisms = startingNumOrganisms;
	}
	
	public void setPopulation(double avgDailyPopulation)
	{
		this.avgDailyPopulation = avgDailyPopulation;
	}
	
	public void setDays(int daysMultiplied)
	{
		this.daysMultiplied = daysMultiplied;
	}
	
	//accessor
	public int getOrganisms()
	{
		return startingNumOrganisms;
	}
	
	public double getPopulation()
	{
		return avgDailyPopulation;
	}
	
	public int getDays()
	{
		return daysMultiplied;
	}
	
	//method that uses a loop to display the size of the population for each day
	public int displayPopulation()
	{		
		for (int i = 1; i <= daysMultiplied; i++)
		{
			System.out.println("Day: " + i + " / " + "Population: " + startingNumOrganisms);
			startingNumOrganisms += (startingNumOrganisms * avgDailyPopulation);
		}
		return startingNumOrganisms;
	}
}
