package ex8;
import java.util.Scanner;

public class Greatest 
{
	public static void main(String[] args)
	{
		//variables
		int integers;
		int sentinel = -99;
		
		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user to enter a series of integers until they enter -99
		System.out.println("Enter a series of integers. (Enter -99 to end)");
		integers = keyboard.nextInt();
		
		//variables for small to big integers
		int smallNum = integers;
		int bigNum = integers;
		
		while (integers != sentinel)
		{
			System.out.println("Enter a series of integers. (Enter -99 to end)");
			integers = keyboard.nextInt();
			if (integers != sentinel) 
			{
				if (integers < smallNum)
				{
					smallNum = integers;
				}
				else if (integers > bigNum)
				{
					bigNum = integers;
				}
			}
		}
		
		System.out.println("Smallest Number: " + smallNum + " / Largest Number: " + bigNum);
	}
}
