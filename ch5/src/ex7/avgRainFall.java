package ex7;
import java.util.Scanner;

public class avgRainFall 
{
	public static void main(String[] args)
	{
		//variables
		int yearsInput = 0;
		double inchesInput = -1;
		double totalRain = 0;
		
		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user
		while (yearsInput < 1)
		{
			System.out.println("Enter number of years");
			yearsInput = keyboard.nextInt();
		}
		
		//outer loop will iterate once for each year
		for (int years = 1; years <= yearsInput; years++)
		{
			//inner loop will iterate 12 times, once for each month
			for (int month = 1; month <= 12; month++)
			{
				while (inchesInput < 0)
				{
					System.out.println("Enter inches of rainfall for month " + month);
					inchesInput = keyboard.nextDouble();
					totalRain += inchesInput;
				}
				inchesInput = -1;
			}
		}
		
		int totalMonths = yearsInput * 12;
		System.out.println("Number of months: " + totalMonths + " monthes");
		System.out.println("Total Inches: " + totalRain + " inches");
		System.out.println("Average rainfall per month: " + totalRain / totalMonths + " inches");
	}
}
