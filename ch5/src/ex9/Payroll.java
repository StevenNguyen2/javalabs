package ex9;

public class Payroll {
	//instance fields
	private int idNum;
	private double grossPay;
	private double stateTax;
	private double federalTax;
	private double ficaWithholdings;
	
	//mutator
	public void setIdNum(int idNum)
	{
		this.idNum = idNum;
	}
	
	public void setGrossPay(double grossPay)
	{
		this.grossPay = grossPay;
	}
	
	public void setStateTax(double stateTax)
	{
		this.stateTax = stateTax;
	}
	
	public void setFederalTax(double federalTax)
	{
		this.federalTax = federalTax;
	}
	
	public void setFica(double ficaWithholdings)
	{
		this.ficaWithholdings = ficaWithholdings;
	}
	
	//accessor
	public int getIdNum()
	{
		return idNum;
	}
	
	public double getGrossPay()
	{
		return grossPay;
	}
	
	public double getStateTax()
	{
		return stateTax;
	}
	
	public double getFederalTax()
	{
		return federalTax;
	}
	
	public double getFica()
	{
		return ficaWithholdings;
	}
	
	//method to calculate employee's net pay
	
}
