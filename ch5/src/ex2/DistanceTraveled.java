package ex2;

public class DistanceTraveled 
{
	//instance fields
	private double speed;
	private double hoursTraveled;
	
	//mutator
	public void setSpeed(double speed)
	{
		this.speed = speed;
	}
	
	public void setHours(double hoursTraveled)
	{
		this.hoursTraveled = hoursTraveled;
	}
	
	//accessor
	public double getSpeed()
	{
		return speed;
	}
	
	public double getHours()
	{
		return hoursTraveled;
	}
	
	//method to return distance that the vehicle has traveled
	public double calcDistance()
	{
		double distance = 0;
		distance = speed * hoursTraveled;
		return distance;
	}
}
