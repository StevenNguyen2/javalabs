package ex4;
import java.util.Scanner;

public class Pennies {
	//instance fields
	public static void main(String[] args)
	{
		//variables
		int days = 0;
		double salary = 0.01;
		double total = 0;
		
		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user
		while (days < 1) //validation (don't accept number less than 1)
		{
			System.out.println("Enter amount of days");
			days = keyboard.nextInt();
		}
			
		//output
		for (int i = 1; i <= days; ++i)
		{
			System.out.printf("Day: " + i + " / " + "Total: $%,.2f\n", salary);
			salary *= 2;
			total += salary;
			//salary = Math.pow(2, salary);
		}
		System.out.printf("Total: $%,.2f\n", total/2);
	}
}
