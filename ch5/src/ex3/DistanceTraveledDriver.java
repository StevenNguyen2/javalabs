package ex3;
import java.io.*;
import java.util.Scanner;

public class DistanceTraveledDriver {

	public static void main(String[] args) throws IOException {
		PrintWriter distanceTraveled = new PrintWriter("distanceTraveled.txt");
		
		//variables
		double speedInput = -1;
		double timeInput = 0; 
		
		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user
		while (speedInput < 0)
		{
			System.out.println("Enter speed (in mph)");
			speedInput = keyboard.nextDouble();
		}
		
		while (timeInput < 1) 
		{
			System.out.println("Enter amount of time spent traveling (in hours)");
			timeInput = keyboard.nextDouble();
		}
		
		//instantiate an object and set attributes
		DistanceTraveled d1 = new DistanceTraveled();
		d1.setSpeed(speedInput);
		d1.setHours(timeInput);
		
		//output
		for (int hours = 1; hours <= timeInput; hours++)
		{
			//not calling method (speedInput * hours instead) cause hours changes based on how far the expression will evaluate
			distanceTraveled.println("Hour: " + hours + " / " + "Distance Traveled: " + (speedInput * hours));
		}
		distanceTraveled.close();
	}

}
