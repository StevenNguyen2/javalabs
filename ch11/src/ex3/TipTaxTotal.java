package ex3;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import java.text.NumberFormat;


public class TipTaxTotal extends Application{
	//variables
	Label lblCharge;
	Label lblTip;
	Label lblTax;
	Label lblTotal;
	
	TextField chargeText;
	
	String input;
	
	int result;
	double tipResult;
	double salesResult;
	double totalResult;
	
	public static void main(String[] args)
	{
		//launch the application
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("Tip, Tax, and Total");
		
		//set label
		lblCharge = new Label("Enter food charge: ");
		lblTip = new Label("18% Tip: ");
		lblTax = new Label("7% Sales Tax: ");
		lblTotal = new Label("Total: ");
		
		//create Button
		Button btn1 = new Button("Calculate");
		
		//Create empty TextField
		chargeText = new TextField();
		
		VBox vbox = new VBox(lblCharge, chargeText, btn1, lblTip, lblTax, lblTotal);
		
		//register an event handler
		btn1.setOnAction(new ButtonClickHandler());
		
		//create scene
		Scene scene = new Scene(vbox);
		
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	private class ButtonClickHandler implements EventHandler<ActionEvent>
	{

		@Override
		public void handle(ActionEvent event) {
			//get a currency formatter for the current locale
			NumberFormat fmt = NumberFormat.getCurrencyInstance();
			
			//retrieve the text that the user has typed into a TextField control
			input = chargeText.getText();
			
			//math for 18 percent tip
			result = Integer.parseInt(input);
			tipResult = (result * .18);
			
			//math for 7 percent sales tax
			salesResult = (result * .07);
			
			//math for total of all 3 amounts
			totalResult = result + tipResult + salesResult;
			
			//output to labels
			lblTip.setText("18% Tip: " + fmt.format(tipResult));
			lblTax.setText("7% Sales Tax: " + fmt.format(salesResult));
			lblTotal.setText("Total: " + fmt.format(totalResult));
			
		}
	}
}
