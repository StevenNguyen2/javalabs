package ex1;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class LatinTranslator extends Application{
	Label messageLabel;
	
	public static void main(String[] args)
	{
		//launch the application
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		//Set the stage title
		primaryStage.setTitle("Latin Translator");
		
		//Show the window
		//primaryStage.show();
		

		//Create a Label
		messageLabel = new Label("");
		
		//Create a Button control
		Button sinister = new Button("Sinister");
		Button dexter = new Button("Dexter");
		Button medium = new Button("Medium");
		
		//Register an event handler
		sinister.setOnAction(new ButtonClickHandler());
		dexter.setOnAction(new ButtonClickHandler2());
		medium.setOnAction(new ButtonClickHandler3());
		
		
		//Create a VBox container and add the Buttons
		VBox vbox = new VBox(10, sinister, dexter, medium, messageLabel);
		
		//Create a Scene and add the VBox as the root node
		Scene scene = new Scene(vbox, 100, 200);
		
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	class ButtonClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent event)
		{
			messageLabel.setText("Left");
		}
	}
	
	class ButtonClickHandler2 implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent event)
		{
			messageLabel.setText("Right");
		}
	}
	
	class ButtonClickHandler3 implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent event)
		{
			messageLabel.setText("Center");
		}
	}
}
