package ex5;

import java.util.Random;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class HeadsOrTails extends Application{
	Image heads1 = new Image("file:C:\\Users\\Steven Nguyen\\Desktop\\heads.jpg");
	Image tails1 = new Image("file:C:\\Users\\Steven Nguyen\\Desktop\\tails.jpg");
	ImageView showHeads = new ImageView();


	public static void main(String[] args)
	{
		//launch the application
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		primaryStage.setTitle("Heads or Tails");
		primaryStage.setHeight(300);
		primaryStage.setWidth(300);
		
		//create a button
		Button btn1 = new Button("Flip a coin");
				
		//register an event handler
		btn1.setOnAction(new ButtonClickHandler());
		
		//create a VBox
		VBox vbox = new VBox(btn1, showHeads);
		
		Scene scene = new Scene(vbox);
		
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	class ButtonClickHandler implements EventHandler<ActionEvent>
	{

		@Override
		public void handle(ActionEvent arg0) {
			// TODO Auto-generated method stub
			Random rng = new Random();
			
			int result = rng.nextInt(2);
			//int heads = 0;
			//int tails = 1;
						
			if (result == 0)
			{
				showHeads.setImage(heads1);
			}
			else
			{
				showHeads.setImage(tails1);
			}
		}
		
	}
}
