package ex7;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.event.ActionEvent;
import java.text.NumberFormat;
import javafx.scene.layout.GridPane;

public class TravelExpenses extends Application{
	//variables
	Label lblDays;
	Label lblAirfare;
	Label lblRental;
	Label lblMiles;
	Label lblParking;
	Label lblTaxi;
	Label lblRegistration;
	Label lblLodging;
	Label lblTotal;
	Label lblAllowable;
	Label lblExcess;
	Label lblAmountSaved;
	
	TextField tfDays;
	TextField tfAirfare;
	TextField tfRental;
	TextField tfMiles;
	TextField tfParking;
	TextField tfTaxi;
	TextField tfRegistration;
	TextField tfLodging;
	
	
	public static void main(String[] args)
	{
		//launch the application
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		//set the title
		primaryStage.setTitle("Travel Expenses");
		
		//create labels
		lblDays = new Label("Number of days on the trip ");
		lblAirfare = new Label("Amount of airfare");
		lblRental = new Label("Amount of car rental fees");
		lblMiles = new Label("Number of miles driven");
		lblParking = new Label("Amount of parking fees");
		lblTaxi = new Label("Amount of taxi charges");
		lblRegistration = new Label("Conference/Seminar registration fees");
		lblLodging = new Label("Lodging charges, per night");
		lblTotal = new Label("Total Expenses Incurred:");
		lblAllowable = new Label("Total Allowable Expenses:");
		lblExcess = new Label("Excess that must be paid by the business person:");
		lblAmountSaved = new Label("Amount saved if the expenses were under the total allowed:");
		
		//create TextFields
		tfDays = new TextField();
		tfAirfare = new TextField();
		tfRental = new TextField();
		tfMiles = new TextField();
		tfParking = new TextField();
		tfTaxi = new TextField();
		tfRegistration = new TextField();
		tfLodging = new TextField();
		
		//create a button
		Button btn1 = new Button("Calculate");
		
		//create a GridPane
		//VBox vbox = new VBox(lblDays, tfDays, lblAirfare, )
		GridPane gridPane = new GridPane();
		
		//GridPane padding and horizontal/vertical spacing
		gridPane.setPadding(new Insets(20));
		gridPane.setHgap(10);
		gridPane.setVgap(10);
		
		//add labels to GridPane
		gridPane.add(lblDays, 0, 0, 1, 1);
		gridPane.add(lblAirfare, 0, 1, 1, 1);
		gridPane.add(lblRental, 0, 2, 1, 1);
		gridPane.add(lblMiles, 0, 3, 1, 1);
		gridPane.add(lblParking, 0, 4, 1, 1);
		gridPane.add(lblTaxi, 0, 5, 1, 1);
		gridPane.add(lblRegistration, 0, 6, 1, 1);
		gridPane.add(lblLodging, 0, 7, 1, 1);
		gridPane.add(lblTotal, 0, 8, 1, 1);
		gridPane.add(lblAllowable, 0, 9, 1, 1);
		gridPane.add(lblExcess, 0, 10, 1, 1);
		gridPane.add(lblAmountSaved, 0, 11, 1, 1);
		
		//add TextFields to GridPane
		gridPane.add(tfDays, 2, 0, 1, 1);
		gridPane.add(tfAirfare, 2, 1, 1, 1);
		gridPane.add(tfRental, 2, 2, 1, 1);
		gridPane.add(tfMiles, 2, 3, 1, 1);
		gridPane.add(tfParking, 2, 4, 1, 1);
		gridPane.add(tfTaxi, 2, 5, 1, 1);
		gridPane.add(tfRegistration, 2, 6, 1, 1);
		gridPane.add(tfLodging, 2, 7, 1, 1);

		//create a Scene
		Scene scene = new Scene(gridPane);
		
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	private class ButtonClickHandler implements EventHandler<ActionEvent>
	{

		@Override
		public void handle(ActionEvent arg0) {
			
		}
		
	}
}
