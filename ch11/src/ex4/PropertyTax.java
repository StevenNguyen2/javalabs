package ex4;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import java.text.NumberFormat;


public class PropertyTax extends Application{
	//variables
	Label lblValue;
	Label lblAssessment;
	Label lblPropertyTax;
	TextField text1;
	String input;
	int result;
	double assessResult;
	double cost;
	double actualTotal;
	
	public static void main(String[] args)
	{
		//launch the application
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		//set the title
		primaryStage.setTitle("Property Tax");
		
		//create a label
		lblValue = new Label("Actual Value:");
		lblAssessment = new Label("Assessment Value: ");
		lblPropertyTax = new Label("Property Tax: ");
		
		//create an empty TextField
		text1 = new TextField();
		
		//create a button
		Button btn1 = new Button("Calculate");
		
		//create a VBox
		VBox vbox = new VBox(lblValue, text1, btn1, lblAssessment, lblPropertyTax);
		
		//register an event handler
		btn1.setOnAction(new ButtonClickHandler());
		
		//create a scene
		Scene scene = new Scene(vbox);
		
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	private class ButtonClickHandler implements EventHandler<ActionEvent>
	{

		@Override
		public void handle(ActionEvent event) {
			//get a currency formatter for the current locale
			NumberFormat fmt = NumberFormat.getCurrencyInstance();
			
			//retrieve text that the user has typed into a TextField control
			input = text1.getText();
			
			//math for assessment value (60% of actual value)
			result = Integer.parseInt(input);
			assessResult = result * .60;
			
			//math for property tax ($0.64 for each $100)
			cost = assessResult / 100;
			actualTotal = cost * .64;
			
			//output to labels
			lblAssessment.setText("Assessment Value: " + fmt.format(assessResult));
			lblPropertyTax.setText("Property Tax: " + fmt.format(actualTotal));
		}
		
	}
	
}
