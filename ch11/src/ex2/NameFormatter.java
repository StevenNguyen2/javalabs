package ex2;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class NameFormatter extends Application{	
	//variables
	String firstInput;
	String middleInput;
	String lastInput;
	String titleInput;
	
	TextField firstName;
	TextField middleName;
	TextField lastName;
	TextField title;
	
	Label lbl1;
	Label lbl2;
	Label lbl3;
	Label lbl4;
	Label lbl5;
	Label lbl6;
	
	Button btn7 = new Button("Format 1");
	Button btn2 = new Button("Format 2");
	Button btn3 = new Button("Format 3");
	Button btn4 = new Button("Format 4");
	Button btn5 = new Button("Format 5");
	Button btn6 = new Button("Format 6");


	
	public static void main(String[] args)
	{
		//launch the application
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception
	{	
		primaryStage.setTitle("Name Formatter");
		
		//create labels
		Label firstLbl = new Label("First Name: ");
		Label middleLbl = new Label("Middle Name: ");
		Label lastLbl = new Label("Last Name: ");
		Label titleLbl = new Label("Title: ");
		//lbl1 = new Label("");
		//lbl2 = new Label("");
		//lbl3 = new Label("");
		//lbl4 = new Label("");
		//lbl5 = new Label("");
		//lbl6 = new Label("");

		//Create button
		Button btn1 = new Button("Format");
		
		//Create empty TextField and retrieve text that the user has typed into a Textfield control
		firstName = new TextField();
		middleName = new TextField();
		lastName= new TextField();
		title = new TextField();
		
		//Create a VBox
		//VBox labelBox = new VBox(firstLbl, firstName, middleLbl, middleName, lastLbl, lastName, titleLbl, title, btn1, lbl1, lbl2, lbl3, lbl4, lbl5, lbl6);
		VBox labelBox = new VBox(firstLbl, firstName, middleLbl, middleName, lastLbl, lastName, titleLbl, title, btn1, btn7, btn2, btn3, btn4, btn5, btn6);

		
		//Register an event handler
		btn1.setOnAction(new ButtonClickHandler());
		
		//Create scene
		Scene scene = new Scene(labelBox);
		
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	private class ButtonClickHandler implements EventHandler<ActionEvent>
	{

		@Override
		public void handle(ActionEvent event) {
			//retrieve text that the user has typed into a TextField control
			firstInput = firstName.getText();
			middleInput = middleName.getText();
			lastInput = lastName.getText();
			titleInput = title.getText();
			
			//lbl1.setText(titleInput + " " + firstInput + " " + middleInput + " " + lastInput);
			//lbl2.setText(firstInput + " " + middleInput + " " + lastInput);
			//lbl3.setText(firstInput + " " + lastInput);
			//lbl4.setText(lastInput + " " + firstInput + " " + middleInput + " " + titleInput);
			//lbl5.setText(lastInput + " " + firstInput + " " + middleInput);
			//lbl6.setText(lastInput + " " + firstInput);
			
			btn7.setText(titleInput + " " + firstInput + " " + middleInput + " " + lastInput);
			btn2.setText(firstInput + " " + middleInput + " " + lastInput);
			btn3.setText(firstInput + " " + lastInput);
			btn4.setText(lastInput + " " + firstInput + " " + middleInput + " " + titleInput);
			btn5.setText(lastInput + " " + firstInput + " " + middleInput);
			btn6.setText(lastInput + " " + firstInput);
		}
		
	}
}
