package ex1;

public class Employee {
	//INSTANCE FIELDS
	private String name;
	private String employeeNumber;
	private String hireDate;
	
	//CONSTRUCTOR
	public Employee(String n, String num, String date)
	{
		this.name = n;
		this.employeeNumber = num;
		this.hireDate = date;
	}
	
	//DEFAULT CONSTRUCTOR
	public Employee()
	{
		
	}
	
	//MUTATORS
	public void setName(String n)
	{
		this.name = n;
	}
	
	public void setEmployeeNumber(String e)
	{
		this.employeeNumber = e;
	}
	
	public void setHireDate(String h)
	{
		this.hireDate = h;
	}
	
	//ACCESSORS
	public String getName()
	{
		return name;
	}
	
	public String getEmployeeNumber()
	{
		return employeeNumber;
	}
	
	public String getHireDate()
	{
		return hireDate;
	}
	
	public boolean isValidEmpNum(String e)
	{
		boolean validation = true;
		if ((!Character.isDigit(e.charAt(0))) || (!Character.isDigit(e.charAt(1))) || (!Character.isDigit(e.charAt(2))) || (e.charAt(3) != '-') || (!Character.isLetter(e.charAt(4))) || (!(e.charAt(4)>= 'A' && e.charAt(4)<= 'M')))
            {
               validation = false;
            }
		return validation;
	}
	
	public String toString()
	{
		return "Employee name: " + name + "\nEmployee Number: " + employeeNumber + "\nHire Date: " + hireDate;
	}
}
