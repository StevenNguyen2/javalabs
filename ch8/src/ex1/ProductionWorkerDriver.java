package ex1;
import java.util.Scanner;

public class ProductionWorkerDriver {

	public static void main(String[] args) {
		//variable
		String input = "12x-A";
		String nameInput;
		String dateInput;
		int shiftInput;
		double payRate;
		
		
		//create a scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//ProductionWorker p1 = new ProductionWorker("Steven", "123-A", "2/16/18", 1, 9);
		//Employee p1 = new Employee();
		
		ProductionWorker p1 = new ProductionWorker();
		
		//prompt user
		while (p1.isValidEmpNum(input) == false)
			{
				System.out.println("Enter Employee Number");
				input = keyboard.nextLine();
				p1.setEmployeeNumber(input);
			}
		
		System.out.println("Enter name");
		nameInput = keyboard.nextLine();
		p1.setName(nameInput);
		
		System.out.println("Enter hire date");
		dateInput = keyboard.nextLine();
		p1.setHireDate(dateInput);
		
		System.out.println("Enter shift");
		shiftInput = keyboard.nextInt();
		p1.setShift(shiftInput);
		
		System.out.println("Enter pay rate");
		payRate = keyboard.nextDouble();
		p1.setPayRate(payRate);
				
		//output
		//System.out.println("Employee Number: " + p1.getEmployeeNumber());
		//System.out.println("Name: " + nameInput);
		//System.out.println("Hire Date: " + dateInput);
		//System.out.println(p1.getShift());
		//System.out.println(p1.getPayRate());
		System.out.println(p1);
		
		keyboard.close();
	}

}
