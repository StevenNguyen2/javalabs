package ex1;

public class ProductionWorker extends Employee {
	//INSTANCE FIELDS
	private int shift;
	private double payRate;
	
	public int DAY_SHIFT = 1;
	public int NIGHT_SHIFT = 2;
	
	//CONSTRUCTOR
	public ProductionWorker(String n, String num, String date, int sh, double rate)
	{
		super(n, num, date);
		shift = sh;
		payRate = rate;
	}
	
	//DEFAULT CONSTRUCTOR
	public ProductionWorker()
	{
		
	}
	
	//MUTATORS
	public void setShift(int s)
	{
		shift = s;
	}
	
	public void setPayRate(double p)
	{
		payRate = p;
	}
	
	//ACCESSORS
	public int getShift()
	{
		return shift;
	}
	
	public double getPayRate()
	{
		return payRate;
	}
	
	public String toString()
	{
		String random = super.toString();
		
		random += "\nShift: ";
		
		if (shift == DAY_SHIFT)
		{
			random += "Day Shift";
		}
		else if (shift == NIGHT_SHIFT)
		{
			random += "Night Shift";
		}
		
		random += "\nHourly Pay Rate: $" + payRate;
		return random;
	}
}
