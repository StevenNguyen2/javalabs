package ex2;

public class Employee {
	//INSTANCE FIELDS
	private String name;
	private String employeeNumber;
	private String hireDate;
	
	//CONSTRUCTOR
	public Employee(String n, String num, String date)
	{
		this.name = n;
		this.employeeNumber = num;
		this.hireDate = date;
		
		if(isValidEmpNum(employeeNumber) == true)
		{
			employeeNumber = num;
		}
		else
		{
			employeeNumber = "false";
		}
	}
	
	//DEFAULT/NO ARGS CONSTRUCTOR
	public Employee()
	{
		
	}
	
	//MUTATORS
	public void setName(String n)
	{
		name = n;
	}
	
	public void setHireDate(String h)
	{
		hireDate = h;
	}
	
	//ACCESSORS
	public String getName()
	{
		return name;
	}
	
	public String getEmployeeNumber()
	{
		return employeeNumber;
	}
	
	public String getHireDate()
	{
		return hireDate;
	}
	
	private boolean isValidEmpNum(String e)
	{
		boolean validation = true;
		if ((!Character.isDigit(e.charAt(0))) || (!Character.isDigit(e.charAt(1))) || (!Character.isDigit(e.charAt(2))) || (e.charAt(3) != '-') || (!Character.isLetter(e.charAt(4))) || (!(e.charAt(4)>= 'A' && e.charAt(4)<= 'M')))
            {
               validation = false;
            }
		return validation;
	}
	
	public String toString()
	{
		return "Name: " + name + "\nEmployee Nunber: " + employeeNumber + "\nHire Date: " + hireDate;
	}
}
