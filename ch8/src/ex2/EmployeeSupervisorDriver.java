package ex2;
import java.util.Scanner;

public class EmployeeSupervisorDriver {

	public static void main(String[] args) {
		//VARIABLES
		String input;
		String nameInput;
		String dateInput;
		double salaryInput;
		double bonusInput;
		
		//create a scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user
		System.out.println("Enter name: ");
		nameInput = keyboard.nextLine();
		
		System.out.println("Enter hire date: ");
		dateInput = keyboard.nextLine();
		
		System.out.println("Enter Employee Number: ");
		input = keyboard.nextLine();
		
		System.out.println("Enter salary");
		salaryInput = keyboard.nextDouble();
		
		System.out.println("Enter bonus");
		bonusInput = keyboard.nextDouble();

		//instantiate an object
		ShiftSupervisor e1 = new ShiftSupervisor(nameInput, input, dateInput, salaryInput, bonusInput);
		System.out.println(e1);

		keyboard.close();
	}

}
