package ex2;

public class ShiftSupervisor extends Employee{
	//INSTANCE FIELDS
	private double salary;
	private double bonus;
	
	//CONSTRUCTOR
	public ShiftSupervisor(String n, String num, String date, double sal, double b)
	{
		super(n, num, date);
		this.salary = sal;
		this.bonus = b;
	}
	
	//DEFAULT/NO ARGS CONSTRUCTOR
	public ShiftSupervisor()
	{
		
	}
	
	//MUTATORS
	public void setSalary(double s) 
	{
		salary = s;
	}
	
	public void setBonus(double b)
	{
		bonus = b;
	}
	
	//ACCESSORS
	public double getSalary()
	{
		return salary;
	}
	
	public double getBonus()
	{
		return bonus;
	}
	
	public String toString()
	{
		String random = super.toString();
		
		//random += "\nBonus: $%.2f" + bonus;
		//random += "\nSalary: $%.2f" + salary;
		//return random;
		
		//random += String.format("\nBonus: $%.2f", bonus);
		//random += String.format("\nSalary: $%.2f", salary);
		//return random;
		
		
		return super.toString() + (String.format("\nSalary: $%,.2f \nBonus: $%,.2f", salary, bonus));
	}
}
