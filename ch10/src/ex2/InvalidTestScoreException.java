package ex2;

public class InvalidTestScoreException extends Exception{
	public InvalidTestScoreException()
	{
		super("Test Score is invalid");
	}
}
