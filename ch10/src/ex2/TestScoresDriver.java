package ex2;
import java.util.Scanner;

public class TestScoresDriver {
	public static void main(String[] args)
	{
		//variables
		int numOfScores;
		
		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user
		System.out.println("How many scores are you entering?");
		numOfScores = keyboard.nextInt();
		
		int[] testScores = new int[numOfScores];
		
		for (int i = 0; i < numOfScores; i++)
		{
			System.out.println("Enter score:");
			testScores[i] = keyboard.nextInt();
		}
		
		//try catch
		try
		{
			//instantiate the object
			TestScores t1 = new TestScores(testScores, numOfScores);
			System.out.println("Average Scores: " + t1.returnAvgTestScore());
			//throw new InvalidTestScoreException();
		}
		catch (InvalidTestScoreException e)
		{
			System.out.println("You have entered an invalid test score");
		}
	}
}
