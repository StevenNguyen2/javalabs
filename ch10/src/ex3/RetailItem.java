package ex3;

import ex3.InvalidPriceException;
import ex3.InvalidUnitsOnHand;

public class RetailItem {
	//instance fields
	private String description;
	private int unitsOnHand;
	private double price;
	
	//constructor
	public RetailItem(String description, int unitsOnHand, double price) throws InvalidPriceException, InvalidUnitsOnHand
	{
		if (price < 0)
		{
			throw new InvalidPriceException();
		}
			
		if (unitsOnHand < 0)
		{

		}
		
		this.description = description;
		this.unitsOnHand = unitsOnHand;
		this.price = price;
		}
	
	//mutator
	public void setDescription(String description)
	{
		this.description = description;
	}
	
	public void setUnits(int unitsOnHand)
	{
		this.unitsOnHand = unitsOnHand;
	}
	
	public void setPrice(double price)
	{
		this.price = price;
	}
	
	//accessor
	public String getDescription()
	{
		return description;
	}
	
	public int getUnits()
	{
		return unitsOnHand;
	}
	
	public double getPrice()
	{
		return price;
	}
}
