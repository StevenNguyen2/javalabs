package ex3;

public class InvalidUnitsOnHand extends Exception{
	public InvalidUnitsOnHand()
	{
		super("Invalid number of units");
	}
}
