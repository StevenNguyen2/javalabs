package ex3;

public class RetailItemDriver {

	public static void main(String[] args) throws InvalidPriceException, InvalidUnitsOnHand{
		//instantiate an object
		RetailItem item1 = new RetailItem("Jacket", 12, 59.95);
		RetailItem item2 = new RetailItem("Designer Jeans", -40, 34.95);
		RetailItem item3 = new RetailItem("Shirt", 20, -24.95);
		
		//set attributes
		
		
		//call getters
		System.out.println(item1.getDescription());
		System.out.println(item1.getUnits());
		System.out.println(item1.getPrice());
		
		System.out.println("");
		
		System.out.println(item2.getDescription());
		System.out.println(item2.getUnits());
		System.out.println(item2.getPrice());
		
		System.out.println("");
		
		System.out.println(item3.getDescription());
		System.out.println(item3.getUnits());
		System.out.println(item3.getPrice());
	}

}
