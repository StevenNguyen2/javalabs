package ex3;

public class InvalidPriceException extends Exception{
	public InvalidPriceException()
	{
		super("Invalid price");
	}
}
