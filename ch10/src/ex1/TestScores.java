package ex1;

public class TestScores {
	//instance fields
		private int[] testScores;
		
		//constructor
		public TestScores(int[] testScores, int size) throws IllegalArgumentException
		{
			//this.testScores = testScores;
			this.testScores = new int[size];
			for (int i = 0; i < testScores.length; i++)
			{
				if (testScores[i] < 0 || testScores[i] > 100)
				{
					throw new IllegalArgumentException("Test scores are invalid");
				}
				else
				{
					this.testScores[i] = testScores[i];
				}
			}
		}
		
		//method
		public int returnAvgTestScore()
		{
			int sum = 0;
			int avg = 0;
			
			for(int i = 0; i < testScores.length; i++)
			{
				sum += testScores[i];
			}
			avg = sum / testScores.length;
			return avg;
		}
}
