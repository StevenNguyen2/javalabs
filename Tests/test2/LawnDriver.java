package test2;
import java.util.Scanner;

public class LawnDriver {

	public static void main(String[] args) {
		//variables
		double length1;
		double length2;
		double width1;
		double width2;
		
		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user
		System.out.println("Enter length of first lawn");
		length1 = keyboard.nextDouble();
		System.out.println("Enter width of first lawn");
		width1 = keyboard.nextDouble();
		
		System.out.println("Enter length of second lawn");
		length2 = keyboard.nextDouble();
		System.out.println("Enter width of second lawn");
		width2 = keyboard.nextDouble();
		
		//create instance of Lawn class using constructor w/ 2 parameters
		Lawn lawn1 = new Lawn(width1, length1);
		
		//create instance of Lawn class using mutator
		Lawn lawn2 = new Lawn();
		lawn2.setLength(length2);
		lawn2.setWidth(width2);
		
		
		//lawn1 output
		double fee1 = 0;
		
		if(lawn1.calcSquareFt() < 400) 
		{
			fee1 = 25;
			System.out.printf("Weekly Fee for lawn 1: $%,.2f\n", fee1);
		}
		else if(lawn1.calcSquareFt() >= 400 && lawn1.calcSquareFt() < 600)
		{
			fee1 = 35;
			System.out.printf("Weekly Fee for lawn 1: $%,.2f\n", fee1);

		}
		else if(lawn1.calcSquareFt() >= 600) 
		{
			fee1 = 50;
			System.out.printf("Weekly Fee for lawn 1: $%,.2f\n", fee1);
		}
		
		//lawn2 output
		double fee2 = 0;
		if(lawn2.calcSquareFt() < 400) 
		{
			fee2 = 25;
			System.out.printf("Weekly Fee for lawn 2: $%,.2f\n", fee2);
		}
		else if(lawn2.calcSquareFt() >= 400 && lawn2.calcSquareFt() < 600)
		{
			fee2 = 35;
			System.out.printf("Weekly Fee for lawn 2: $%,.2f\n", fee2);

		}
		else if(lawn2.calcSquareFt() >= 600) 
		{
			fee2 = 50;
			System.out.printf("Weekly Fee for lawn 2: $%,.2f\n", fee2);
		}
		
		//total fees output
		System.out.printf("Lawn 1 Total Fees: $%,.2f\n", (fee1 * 20));
		System.out.printf("Lawn 2 Total Fees: $%,.2f\n", (fee2 * 20));
	}
}
