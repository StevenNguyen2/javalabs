package test2;

public class Lawn {
	//instance fields
	private double width;
	private double length;
	
	//constructor
	public Lawn(double width, double length)
	{
		this.width = width;
		this.length = length;
	}
	
	//default constructor
	public Lawn()
	{
		
	}
	
	//mutator
	public void setWidth(double width)
	{
		this.width = width;
	}
	
	public void setLength(double length)
	{
		this.length = length;
	}
	
	//accessor
	public double getWidth()
	{
		return width;
	}
	
	public double getLength()
	{
		return length;
	}
	
	//method to calculate and return square ft of a lawn
	public double calcSquareFt()
	{
		double area = 0;
		double fee = 0;
		area = length * width;
		
		return area;
	}
}
