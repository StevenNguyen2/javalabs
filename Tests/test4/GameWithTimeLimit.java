package test4;

public class GameWithTimeLimit extends Game{
	//variables
	int timeLimit;
	
	//constructor
	public GameWithTimeLimit(String game, int maxNum, int timeLimit)
	{
		//inheritance
		super(game, maxNum);
		this.timeLimit = timeLimit;
	}
	//mutators
	public void setTimeLimit(int timeLimit)
	{
		this.timeLimit = timeLimit;
	}
	
	//accessors
	public int getTimeLimit()
	{
		return timeLimit;
	}
	
	//toString
	public String toString()
	{
		return super.toString() + "\nTime Limit: " + timeLimit;
	}
}
