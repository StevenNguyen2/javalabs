package test4;

public class Vehicle {
	//instance fields
	int speed;
	
	//accelerate method
	public int accelerate()
	{
		speed += 5;
		return speed;
	}
	
	//accessor
	public int getSpeed()
	{
		return speed;
	}
}
