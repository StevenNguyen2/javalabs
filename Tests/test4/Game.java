package test4;

public class Game {
	//variables
	String game;
	int maxNum;
	
	public Game(String game, int maxNum)
	{
		this.game = game;
		this.maxNum = maxNum;
	}
	
	//mutators
	public void setGame(String game)
	{
		this.game = game;
	}
	
	public void setMaxNum(int maxNum)
	{
		this.maxNum = maxNum;
	}
	
	//accessors
	public String getGame()
	{
		return game;
	}
	
	public int getMaxNum()
	{
		return maxNum;
	}
	
	//toString
	public String toString()
	{
		return "\nGame Name: " + game + "\nNumber of Players: " + maxNum;
	}
}
