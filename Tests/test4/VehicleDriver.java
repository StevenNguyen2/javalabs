package test4;

public class VehicleDriver {

	public static void main(String[] args) {
		Vehicle car = new Car();
		
		
		car.accelerate();
		car.accelerate();
		System.out.println(car.accelerate());
		Vehicle truck = new Truck();
		
		System.out.println(truck.accelerate());
		
		Vehicle vehicle = new Vehicle();
		
		System.out.println(vehicle.accelerate());
	}

}
