package test4;
import java.util.Scanner;

public class GameDriver {

	public static void main(String[] args) {
		//variables
		String name;
		int maxNum;
		int timeLimit;
		
		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user
		System.out.println("Enter name of game");
		name = keyboard.nextLine();
		
		System.out.println("Enter maximum number of players");
		maxNum = keyboard.nextInt();

		System.out.println("Enter time limit");
		timeLimit = keyboard.nextInt();
		
		//instantiate an object
		GameWithTimeLimit g1 = new GameWithTimeLimit(name, maxNum, timeLimit);
		
		//output
		System.out.println(g1);

	}

}
