package test5;

import java.text.NumberFormat;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.VBox;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.event.ActionEvent;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import java.text.NumberFormat;


public class Ranken extends Application{
	//variables
	Label lblOil;
	Label lblLube;
	Label lblRadiator;
	Label lblTransmission;
	Label lblInspection;
	Label lblMuffler;
	Label lblTire;
	Label lblTotal;
	
	TextField parts;
	TextField laborHours;
	
	Button calc;
	Button exit;
	
	CheckBox cbOil;
	CheckBox cbLube;
	CheckBox cbRadiator;
	CheckBox cbTransmission;
	CheckBox cbInspection;
	CheckBox cbMuffler;
	CheckBox cbTire;
	
	double total;
	double resultHours;
	double resultParts;
	double totalHours;
	double totalParts;

	String inputHours;
	String inputParts;
	
	public static void main(String[] args)
	{
		//launch the application
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		primaryStage.setTitle("Ranken's Automotive Maintenance");
		primaryStage.setWidth(400);
		primaryStage.setHeight(400);
		
		//create labels
		Label lblRoutine = new Label("Routine Services");
		Label lblNonRoutine = new Label("Nonroutine Services");
		Label lblPartsCharges = new Label("Parts Charges:");
		Label lblHoursOfLabor = new Label("Hours of Labor:");
		lblTotal = new Label("Total Charges:");
		
		//create TextFields
		parts = new TextField();
		laborHours = new TextField();
		
		//create buttons
		calc = new Button("Calculate Charges");
		exit = new Button("Exit");
		
		//create CheckBox
		cbOil = new CheckBox("Oil Change ($26.00)");
		cbLube = new CheckBox("Lube Job ($18.00)");
		cbRadiator = new CheckBox("Radiator Flush ($30.00)");
		cbTransmission = new CheckBox("Transmission Flush ($80.00)");
		cbInspection = new CheckBox("Inspection ($15.00)");
		cbMuffler = new CheckBox("Muffler Replacement ($100.00)");
		cbTire = new CheckBox("Tire Rotation ($20.00)");
		
		//create a VBox
		VBox vbox = new VBox(lblRoutine, cbOil, lblNonRoutine, lblPartsCharges, lblHoursOfLabor);
		
		//create a GridPane
		GridPane gridPane = new GridPane();
		
		//GridPane padding and horizontal/vertical spacing
		gridPane.setPadding(new Insets(20));
		gridPane.setHgap(10);
		gridPane.setVgap(10);
		
		//add labels & CheckBox to GridPane
		gridPane.add(lblRoutine, 0, 0, 1, 1);
		gridPane.add(cbOil, 0, 1, 1, 1);
		gridPane.add(cbLube, 0, 2, 1, 1);
		gridPane.add(cbRadiator, 0, 3, 1, 1);
		gridPane.add(cbTransmission, 0, 4, 1, 1);
		gridPane.add(cbInspection, 0, 5, 1, 1);
		gridPane.add(cbMuffler, 0, 6, 1, 1);
		gridPane.add(cbTire, 0, 7, 1, 1);
		gridPane.add(lblNonRoutine, 0, 8, 1, 1);
		gridPane.add(lblPartsCharges, 0, 9, 1, 1);
		gridPane.add(lblHoursOfLabor, 0, 10, 1, 1);
		gridPane.add(lblTotal, 2, 0, 1, 1);

		//add TextFields to GridPane
		gridPane.add(parts, 2, 9, 1, 1);
		gridPane.add(laborHours, 2, 10, 1, 1);
		
		//add Buttons to GridPane
		gridPane.add(calc, 0, 11, 1, 1);
		gridPane.add(exit, 2, 11, 1, 1);

		//register an event handler
		exit.setOnAction(new ExitButtonHandler());
		calc.setOnAction(new CalcButtonHandler());
		
		//create a scene
		Scene scene = new Scene(gridPane);
		
		//set scene
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	class ExitButtonHandler implements EventHandler<ActionEvent>
	{

		@Override
		public void handle(ActionEvent arg0) {
			Platform.exit();
		}
	}
	
	class CalcButtonHandler implements EventHandler<ActionEvent>
	{

		@Override
		public void handle(ActionEvent arg0) {
			//get a currency formatter for the current locale			
			NumberFormat fmt = NumberFormat.getCurrencyInstance();

			if (cbOil.isSelected())
			{
				total += 26.00;
			}
			if (cbLube.isSelected())
			{
				total += 18.00;
			}
			if (cbRadiator.isSelected())
			{
				total += 30.00;
			}
			if (cbTransmission.isSelected())
			{
				total += 80.00;
			}
			if (cbInspection.isSelected())
			{
				total += 15.00;
			}
			if (cbMuffler.isSelected())
			{
				total += 100.00;
			}
			if (cbTire.isSelected())
			{
				total += 20.00;
			}
			
			//retrieve the text that the user entered in the TextField
			inputHours = laborHours.getText();
			inputParts = parts.getText();
			
			//math for labor (20 * (labor hours))
			resultHours = Double.parseDouble(inputHours);			
			totalHours = resultHours * 20;
			
			//math for parts charges
			resultParts = Double.parseDouble(inputParts);	
			totalParts += resultParts;
			
			total = total + totalParts + totalHours;
			
			lblTotal.setText("Total Charges: " + fmt.format(total));
			
			//try
			//{
				//double x = Double.parseDouble(inputHours);
				//double y = Double.parseDouble(inputParts);
				//resultHours = Double.parseDouble(inputHours);
				//resultParts = Double.parseDouble(inputParts);
			//}
			//catch (NumberFormatException e)
			//{
				//lblTotal.setText(e.getMessage() + "Invalid input");
				//System.out.println(e.getMessage() + " is not a valid format");
			//}
		}
	}

}
