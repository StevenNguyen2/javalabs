package test1;
import java.util.Scanner;

public class FastFood
{
    public static void main(String[] args)
    {
        //variables
        double numberOfHamburgers;
        double numberOfCheeseburgers;
        double numberOfSodas;
        double numberOfFries;
        double total;
        String name;
        String upperCase;
        
        //create a scanner object for keyboard input
        Scanner keyboard = new Scanner(System.in);
        
        //prompt user
        System.out.println("How many hamburgers?");
        numberOfHamburgers = keyboard.nextDouble();
        
        System.out.println("How many cheeseburgers?");
        numberOfCheeseburgers = keyboard.nextDouble();
        
        System.out.println("How many sodas?");
        numberOfSodas = keyboard.nextDouble();
        
        System.out.println("How many fries");
        numberOfFries = keyboard.nextDouble();
        
        //keyboard buffer
        keyboard.nextLine();
        
        System.out.println("What's your name?");
        name = keyboard.nextLine();
        
        //total calculation
        total = (numberOfHamburgers * 1.25) + (numberOfCheeseburgers * 1.50) + (numberOfSodas * 1.95) + (numberOfFries * .95);
        
        //total output
        System.out.printf("Total: $%,.2f\n", total);
        
        //name output
        upperCase = name.toUpperCase();
        System.out.println("Name: " + upperCase);
    }
}