package test3;

public class Hot3Driver {

	public static void main(String[] args) {
		System.out.println("2 arguments (5 + 5): " + Overloaded.addNum(5, 5));
		System.out.println("3 arguments (1 + 2 + 3): " + Overloaded.addNum(1, 2, 3));
	}

}
