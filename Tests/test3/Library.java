package test3;

public class Library {
	//instance fields
	private String book;
	private Book name;
	
	//2 param constructor
	public Library(Book name, String book)
	{
		this.name = name;
		this.book = book;
	}
	
	public String toString()
	{
		return "\nThe book is " + book + "\nThe name is " + name;
	}
}

