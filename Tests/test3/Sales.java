package test3;
import java.util.Scanner;
import java.io.*;

public class Sales {
	public static void main(String[] args) throws IOException
	{
		PrintWriter sales = new PrintWriter("WeeklySales.txt");
		
		//variables
		int day = 0;
		int total = 0;
		
		//create scanner object for keyboard input
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user
		/*
		while (day <= 0) //don't accept negative numbers (less than 0)
		{
			for (int i = 1; i <= 5; ++i)
			{
				System.out.println("Enter the amount of sales for day " + i);
				day = keyboard.nextInt();
				total += day; //accumulator
				sales.println("Day " + i + ": " + day);
			}
		}
		*/
		
		
		for (int i = 1; i <= 5; ++i)
		{
			while (day >= 0)
			{
				System.out.println("Enter the amount of sales for day " + i);
				day = keyboard.nextInt();
				total += day; //accumulator
				sales.println("Day " + i + ": " + day);
			}
		}

		//output
		sales.println("Total: " + total);
		sales.close();
	}
}
