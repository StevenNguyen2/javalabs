package test3;

public class LibraryBookDriver {

	public static void main(String[] args) {
		Book book1 = new Book("2/12/2018", "Mr. G", "steven");
		Library library1 = new Library(book1, "Book");
		
		System.out.println(library1.toString());
	}
}
