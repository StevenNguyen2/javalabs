package test3;

public class Book {
	//instance fields
	private String name;
	private String publishDate;
	private String author;
	
	//3 param constructor
	public Book(String publishDate, String author, String n)
	{
		this.publishDate = publishDate;
		this.author = author;
		this.name = n;
	}
	
	public String toString()
	{
		return "\nThe name is " + name + "\nThe publish date is " + publishDate + "\nThe author is " + author;
	}
}
